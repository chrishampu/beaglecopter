#ifndef _NET_HTTP_INTERPRETER_H_
#define _NET_HTTP_INTERPRETER_H_

/** This implementation of WebSockets follows RFC6455 (http://tools.ietf.org/html/rfc6455) **/

#include "NetPacket.h"
#include "NetSession.h"
#include <map>

enum WebSocketOpcode
{
	TextFrame = 0x1,
	BinaryFrame,
	CloseFrame = 0x8,
	PingFrame,
	PongFrame
};

enum FrameErrorCode
{
	NormalClosure = 1000,
	GoingAway,
	ProtocolError,
	UnacceptableData,
	Reserved,
	Reserved2,
	Reserved3,
	InconsistantData,
	PolicyViolation,
	OversizedMessage,
	ExtensionNegotiationFail,
	UnexpectedCondition,
};

class NetHTTPInterpreter {
public:
	NetHTTPInterpreter(NetPacket *packet, NetSession *session);
	~NetHTTPInterpreter();

	void InterpretPacket();
	bool InterpretHeaders();
	void AuthenticateHandshake();
	std::string CalcAcceptKey();
	
	static void EncodeAsFrame(NetPacket *packet, WebSocketOpcode opcode);
	static bool isHTTPPacket(NetPacket *packet);

private:
	NetPacket *httpPacket;
	NetSession *httpSession;
	typedef std::map<std::string, std::string> HeaderMap_T;
	HeaderMap_T Headers;
};

#endif