#include "PacketImageTransmit.h"

#include <stdio.h>
#include <string.h>

PacketImageTransmit::PacketImageTransmit()
{
	packetID = pktImageTransmit;
	packetSize = 2;
	dataSize = 0;
}

PacketImageTransmit::PacketImageTransmit(const char *imageData, unsigned short size)
{
	//PacketData.clear();

	packetID = pktImageTransmit;

	data = new char[size];

	memset(this->data, 0, size);
	memcpy(this->data, imageData, size);

	dataSize = size;
	setPacketSize(dataSize + 2 + 2); // msg size + msglen + packetID

	PreparePacket();
}

PacketImageTransmit::~PacketImageTransmit()
{
	if (dataSize > 0)
		delete[] data;
}

void PacketImageTransmit::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << dataSize;
	PacketData.write(reinterpret_cast<unsigned char*>(data), dataSize);
}

void PacketImageTransmit::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketImageTransmit::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> dataSize;

	data = new char[dataSize];

	memset(this->data, 0, dataSize);

	ns.read((unsigned char*)data, dataSize);
}

const char *PacketImageTransmit::getImageData()
{
	return data;
}

unsigned short PacketImageTransmit::getImageSize()
{
	return dataSize;
}
