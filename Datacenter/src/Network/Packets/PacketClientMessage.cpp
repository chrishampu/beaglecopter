#include "PacketClientMessage.h"

#include <stdio.h>
#include <string.h>

PacketClientMessage::PacketClientMessage()
{
	packetID = pktClientMessage;
	packetSize = 2;
	msglen = 0;
}

PacketClientMessage::PacketClientMessage(const char *msg, int len)
{
	packetID = pktClientMessage;

	memset(&this->msg, 0, 256);
	memcpy(&this->msg, msg, len);

	msglen = len;
	setPacketSize(msglen + 2 + 2); // msg size + msglen + packetID

	PreparePacket();
}

void PacketClientMessage::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << msglen;
	PacketData << msg;
}

void PacketClientMessage::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketClientMessage::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> msglen; // Retrieve the length of our message

	memset(&msg, 0, 256); // Initialize the array

	if(msglen > 256) // Sanity check
		return;

	ns.read(msg, msglen); // Read in the message
}

const unsigned char *PacketClientMessage::getMsg()
{
	return msg;
}

unsigned short PacketClientMessage::getMsgLen()
{
	return msglen;
}
