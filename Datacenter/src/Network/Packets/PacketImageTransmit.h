#ifndef _PACKET_IMAGE_H_
#define _PACKET_IMAGE_H_

#include "../NetPacket.h"

class PacketImageTransmit : public NetPacket
{
public:
    PacketImageTransmit();
    PacketImageTransmit(const char *imageData, unsigned short size);
	~PacketImageTransmit();

    virtual void PreparePacket();

    virtual void Handle(NetStream& ns, NetSession* sess);

    virtual void Decode(NetStream& ns);

    const char *getImageData();
    unsigned short getImageSize();

private:
	char *data;
    unsigned short dataSize;
};

#endif