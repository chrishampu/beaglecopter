#pragma once

#include "NetSocket.h"
#include "NetPacketList.h"

#include <boost/signals2.hpp>
#include <queue>

enum NetSessionType {
	Undetermined = 0,
	WebSocket,
	TCPSocket
};

class NetSession
{
public:
	NetSession(NetSocket sock, NetAddress addr);
	~NetSession();

	bool Update();

	void AddPacket(NetPacket *packet);

	// State information for this session
	typedef struct {
		bool RequiresGPSInfo;
		bool RequiresCameraImages;
		bool RequiresPositionInfo;
		bool HasMissionPlanner;
	} SessionState_T;

	SessionState_T SessionState;

private:
	NetSessionType SocketType;
	NetAddress Address;
	NetSocket Socket;

	typedef std::queue<NetPacket*> PacketQueueT;
	PacketQueueT PacketQueue;

public:
	// WebSocket-related variables
	bool HandshakeComplete;

	// Misc set/get functions
	NetSocket getSocket() { return Socket; }

	NetSessionType getSocketType() { return SocketType; }
	void setSocketType(NetSessionType type) { SocketType = type; }

	const SessionState_T &getSessionState() { return SessionState; };

	std::string getAddressString();
};