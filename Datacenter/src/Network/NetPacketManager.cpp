#include "NetPacketManager.h"

#include <Logging/Logging.h>
#include <Common/winConsole.h>

#include <stdio.h>
#include <map>
#include <memory>

std::map<unsigned short, PacketHandle> PacketList;
typedef std::map<unsigned short, PacketHandle>::iterator PacketListIter;

PacketControl *PacketManager = NULL;

/**
	Macro for defining new packets so they can be processed when received.
*/
#define IMPLEMENT(x) \
	void Handle_Packet ##x (NetStream& b, NetSession* sess); \
	static PacketControl Control_Packet ##x(pkt ##x, Handle_Packet ##x); \
	void Handle_Packet ##x (NetStream& b, NetSession* sess) \
	{ \
		Packet ##x *p; \
		p = new Packet ##x (); \
		p->Handle(b, sess); \
		PacketManager->sendPacketSignal(*p); \
		if(p != NULL ) { \
			delete p; \
			p = NULL; \
		} \
	}

/**
	And here we implement all of our packets.
*/
IMPLEMENT(ImageTransmit)
IMPLEMENT(ClientMessage)

PacketControl::PacketControl()
{

}

PacketControl::~PacketControl()
{
	PacketList.clear();
}

PacketControl::PacketControl(unsigned short id, PacketHandle handle)
{
	PacketManager->Register(id, handle);
}

void PacketControl::processPacketReceivedEvent(NetPacket *packet, NetSession *session)
{
	union {
		unsigned short id;
		unsigned char c[2];
	} packetData;

	PacketListIter itr;

	packet->PacketData.peek(packetData.c, 2); // Now we can peek into our stream to grab our packet ID

	itr = PacketList.find(packetData.id); // Attempt to find a handle that matches our packet ID

	if(itr == PacketList.end()) {// Attempt to interpret packet as an HTTP packet
		Console->printf(Networking, "Unknown packet received from %s. Dumping packet.", session->getAddressString().c_str());
		return;
	}

	PacketHandle handle = itr->second; // Grab our handle

	(*handle)(packet->PacketData, session); // Run our handle callback!
}

boost::signals2::connection PacketControl::connectPacketReceiver(const packet_signal_t::slot_type &receiver)
{
	return packet_signal.connect(receiver);
}

void PacketControl::sendPacketSignal(NetPacket &packet)
{
	packet_signal(&packet);
}

void PacketControl::Register(unsigned short id, PacketHandle handle)
{
	PacketList[id] = handle;
}

void PacketControl::Create()
{
	if(PacketManager == NULL)
		PacketManager = new PacketControl();
}

void PacketControl::Destroy()
{
	if(PacketManager != NULL)
		delete PacketManager;
}
