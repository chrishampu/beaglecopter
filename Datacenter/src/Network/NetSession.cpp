#include "NetPlatform.h"
#include "NetSession.h"
#include "NetPacketManager.h"
#include "NetHTTPInterpreter.h"
#include <string>

NetSession::NetSession(NetSocket sock, NetAddress addr)
	: Socket(sock),
	  Address(addr),
      SocketType(Undetermined),
	  HandshakeComplete(false)
{
	memset(&SessionState, 0, sizeof(SessionState));
}

NetSession::~NetSession()
{
	while(!PacketQueue.empty())
	{
		NetPacket* packet = PacketQueue.front();

		delete packet;

		PacketQueue.pop();
	}

	Network->closeSocket(Socket);
}

bool NetSession::Update()
{
	while(!PacketQueue.empty())
	{
		NetPacket* packet = PacketQueue.front();

		if(SocketType == Undetermined)
		{
			if(NetHTTPInterpreter::isHTTPPacket(packet) == true)
			{
				setSocketType(WebSocket);
				
				if(HandshakeComplete == false)
				{
					std::unique_ptr<NetHTTPInterpreter> interpreter(new NetHTTPInterpreter(packet, this));
					interpreter->AuthenticateHandshake();
					continue;
				}
			}
			else
				setSocketType(TCPSocket);
		}

		if(SocketType == WebSocket && HandshakeComplete == true)
		{
			std::unique_ptr<NetHTTPInterpreter> interpreter(new NetHTTPInterpreter(packet, this));
			interpreter->InterpretPacket();
		}
		else
			PacketManager->processPacketReceivedEvent(packet, this);

		delete packet;

		PacketQueue.pop();
	}

	if(SessionState.RequiresPositionInfo)
	{
		// Here we can send any information this session requires
	}

	return true;
}

void NetSession::AddPacket(NetPacket *packet)
{
	PacketQueue.push(packet);
}

std::string NetSession::getAddressString()
{
	return Network->NetAddressToString(&Address);
}