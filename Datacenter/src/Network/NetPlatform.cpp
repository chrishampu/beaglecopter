#include <Common/Platform.h>
#include <Logging/Logging.h>
#include "NetPlatform.h"
#include "NetPacketmanager.h"
#include "NetSessionManager.h"
#include "NetStream.h"

#include <string>
#include <sstream>

#ifdef PLATFORM_WIN
	#include <Common/winConsole.h>
	#include <errno.h>
	#include <winsock.h>
	#define WM_SOCKET_MSG WM_USER
	HWND winsockWindow = NULL;
#endif

#define MaxPacketDataSize 1024

NetPlatform *Network = NULL;

static int defaultPort = 24000;
static int listenPort = 0;
static bool Initialized = false;

const char defaultAddress[16] = "127.0.0.1";
//const char *destinationAddress = serverAddress; // Temporary

/** In a 2-way TCP handshake, 3 sockets are required.
 ** The first socket is the serverside listen socket.
 ** Second, the socket created by the client in order to
 ** keep track of the client -> server connection.
 ** Third, the socket created by the server when a connection
 ** is accepted from a socket. On this socket, data is sent
 ** from the server -> client, and received from the client.
 ** These 3 sockets describe a SYN->SYN ACK->SYN connection.
 **/

ClientSocketObject *ClientSocketObject::first = NULL;
NetSocketObject *ServerSocket = NULL;

#ifdef PLATFORM_WIN
static LRESULT PASCAL WinsockProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int error;
	int event;
	NetSocket socket;
	NetPlatform::Error err;
	int bytesRead;
	unsigned char bytesRecv[MaxPacketDataSize];

	switch(message)
	{
		case WM_SOCKET_MSG:
				error = WSAGETSELECTERROR(lParam);
				event = WSAGETSELECTEVENT(lParam);
				socket = wParam;

				switch(event)
				{
					case FD_READ:
						err = NetPlatform::recv(socket, bytesRecv, MaxPacketDataSize, &bytesRead);
						if(err == NetPlatform::Success && bytesRead != 0)
						{
							NetPacket *packetEvent = new NetPacket();
							NetSession *sess = SessionManager->FindSession(socket);

							packetEvent->PacketData.write(bytesRecv, bytesRead);
							packetEvent->setPacketSize(bytesRead);

							sess->AddPacket(packetEvent);
							Console->printf(Networking, "Received data from %d\r\n", socket);
						}
						break;
					case FD_WRITE:
						Console->printf(Networking, "Recieved write event\r\n");
						break;
					case FD_ACCEPT:
						{
							NetSocket sock = InvalidSocket;
							NetAddress address;

							if((sock = NetPlatform::accept(socket, &address)) != InvalidSocket)
							{
								NetSession *session = new NetSession(sock, address);

								SessionManager->AddSession(session);

								Console->printf(Networking, "Obtained client object using connection socket %d\r\n", sock);

								NetPlatform::setBlocking(sock, false);

								WSAAsyncSelect(sock, winsockWindow, WM_SOCKET_MSG, FD_READ | FD_CONNECT | FD_CLOSE);

								Console->printf(Networking, "Connection accepted from %s\r\n", NetPlatform::NetAddressToString(&address).c_str());
							}
						}
						break;
					case FD_CLOSE:
						{
							Console->printf(Networking, "Recieved close event. Removing session %d\r\n", socket);

							NetSession *session = SessionManager->FindSession(socket);
							if(session != NULL)
								SessionManager->RemoveSession(session);
						}
						break;
					case FD_CONNECT:
						Console->printf(Networking, "Server: Incoming connection from client %d\r\n", socket);
						break;
				}
			break;
			default:
				return DefWindowProc( hWnd, message, wParam, lParam );
	}

	return 0;
}
#endif

// We want the latest version of winsock which is 2.2, so use MAKEWORD(2.2)
bool NetPlatform::Init()
{
	if(Initialized == true)
	{
		Console->printf(Networking, "Fatal error: Network already initialized!\r\n");
		return false;
	}

#ifdef PLATFORM_WIN
	bool success;
	WSADATA WsaDat;

	RegisterWindow();

	if((success = (bool)!WSAStartup(MAKEWORD(2,2), &WsaDat)) == true)
	{
		Console->printf(Networking, "Initializing network. Winsock version: %d.%d\r\n", LOBYTE(WsaDat.wVersion), HIBYTE(WsaDat.wVersion));
		Initialized = true;
	}
#endif

	ServerSocket = new NetSocketObject();

	PacketManager->Create();
	SessionManager->Create();

#ifdef PLATFORM_WIN
	return success;
#elif PLATFORM_LINUX
	return true;
#endif
}

void NetPlatform::Shutdown()
{
	if(Initialized == true)
	{
		Console->printf(Networking, "Network shutting down\r\n");
#ifdef PLATFORM_WIN
		DestroyWindow(winsockWindow);
		WSACleanup();
#endif
		SAFE_DELETE(ServerSocket);

		ClientSocketObject::ObjectCleanup();

		PacketManager->Destroy();
		SessionManager->Destroy();

		Initialized = false;
	}
}

void NetPlatform::Process()
{
#ifdef PLATFORM_WIN
	MSG msg;

	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
	{
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	}
#endif

	SessionManager->UpdateSessions();
}

#ifdef PLATFORM_WIN
void NetPlatform::RegisterWindow()
{
   WNDCLASS wc;
   memset(&wc, 0, sizeof(wc));

   wc.style         = 0;
   wc.lpfnWndProc   = WinsockProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = GetModuleHandle(NULL);
   wc.hIcon         = 0;
   wc.hCursor       = 0;
   wc.hbrBackground = 0;
   wc.lpszMenuName  = 0;
   wc.lpszClassName = "WinSockClass";
   RegisterClass( &wc );
   winsockWindow = CreateWindowEx(
      0,
      "WinSockClass",
      "",
      0,
      0, 0, 0, 0,
      NULL, NULL,
      GetModuleHandle(NULL),
      NULL);
}
#endif

NetSocket NetPlatform::openListenPort(const char *address, unsigned short port)
{
	if(listenPort > 0)
	{
		Console->printf(Networking, "We're already listening on port %d\r\n", listenPort);
		return InvalidSocket;
	}

	if(ServerSocket->socket != InvalidSocket)
	{
		Console->printf(ColorCode::Error, "The network server has already been created\r\n");
		return InvalidSocket;
	}

	NetPlatform::Error err;

	if(port == 0)
		port = defaultPort;

	ServerSocket->socket = openSocket();
	ServerSocket->type = Server;

	Console->printf(Networking, "Listen socket: %d\r\n", ServerSocket->socket);

	if(address == "")
		err = bind(ServerSocket->socket, port);
	else
		err = bind(ServerSocket->socket, port, address);

	if(err != NetPlatform::Success)
	{
		switch(err)
		{
			case NetPlatform::NotASocket:
				Console->printf(ColorCode::Error, "Bind failed: Invalid socket\r\n");
				break;
			case NetPlatform::BadAddress:
				Console->printf(ColorCode::Error, "Bind failed: Invalid address\r\n");
				break;
			default:
				Console->printf(ColorCode::Error, "Bind failed: Unknown error: %d\r\n", err);
				break;
		}

		delete ServerSocket;
		return InvalidSocket;
	}
	
	if(listen(ServerSocket->socket, 1) != NetPlatform::Success)
	{
		Console->printf(Networking, "Listen failed: Unknown error\r\n");
		delete ServerSocket;
		return InvalidSocket;
	}

	setBlocking(ServerSocket->socket, false);

	if(WSAAsyncSelect(ServerSocket->socket, winsockWindow, WM_SOCKET_MSG, FD_ACCEPT))
	{
		Console->printf(Networking, "openListenPort: Connection error: %d\r\n", WSAGetLastError());
		delete ServerSocket;
		return InvalidSocket;
	}
	else
	{
		listenPort = port;

		if(address == "")
		{
			sockaddr_in addr;
			int len = sizeof(addr);
			getsockname(ServerSocket->socket, (sockaddr*)&addr, &len);
			Console->printf(Networking, "Server is listening on %s:%d\r\n", inet_ntoa(addr.sin_addr), port);
		}
		else
			Console->printf(Networking, "Server is listening on %s:%d\r\n", address, port);
	}

	return ServerSocket->socket;
}

NetSocket NetPlatform::openConnectTo(char *stringAddress, unsigned short port)
{
	// Create our socket object.
	// Our objects constructor will add it to a linked list so it won't go out of scope.
	ClientSocketObject *ClientSocket = new ClientSocketObject();

	ClientSocket->socket = openSocket();
	if(ClientSocket->socket == InvalidSocket)
	{
		Console->printf(ColorCode::Error, "Failed to create client socket\r\n");
		return InvalidSocket;
	}

	setBlocking(ClientSocket->socket, false);

	SOCKADDR_IN ipAddr;
	char listenAddress[16];

	// If address is specified as auto, then try to auto detect the local adapter
	if(strncmp(stringAddress, "auto", 4) == 0)
	{
		// Resolve the IP we'll connect to
		if(detectLocalAddress(listenAddress) == NetPlatform::Success)
			ipAddr.sin_addr.s_addr = inet_addr(listenAddress);
		else
			ipAddr.sin_addr.s_addr = INADDR_NONE;
	}
	else
		ipAddr.sin_addr.s_addr = inet_addr(stringAddress);

	if(ipAddr.sin_addr.s_addr != INADDR_NONE)
	{
		ipAddr.sin_port = port == 0 ? htons(defaultPort) : htons(port);
		ipAddr.sin_family = AF_INET;

		// Start an async transaction
		WSAAsyncSelect(ClientSocket->socket, winsockWindow, WM_SOCKET_MSG, FD_READ | FD_CONNECT | FD_CLOSE);

		// Start an async connection
		Console->printf(Networking, "Client: Connecting to %s:%d\r\n", strcmp(stringAddress, "auto")==0 ? listenAddress : stringAddress, port == 0 ? defaultPort : port);
		if(::connect(ClientSocket->socket, (PSOCKADDR) &ipAddr, sizeof(ipAddr)))
		{
			if(NetPlatform::getLastError() != NetPlatform::WouldBlock) // WouldBlock errors are perfectly normal... I think
			{
				Console->printf(Networking, "Client: Connection error: %d\r\n", getLastError());
				::closesocket(ClientSocket->socket);
				SAFE_DELETE(ClientSocket);
				return InvalidSocket;
			}
		}
		Console->printf(Networking, "Client: Connected successfully on socket %d!\r\n", ClientSocket->socket); 
		return ClientSocket->socket;
	}
	else 
	{
		Console->printf(Networking, "openConnectTo: Invalid address\r\n");
		SAFE_DELETE(ClientSocket);
	}

	return InvalidSocket;
}

void NetPlatform::closeConnectTo(NetSocket socket)
{
	::closesocket(socket);
	socket = InvalidSocket;
}

// One feature I can add later is turning bufferSize into a reference pointer and modifying
// it to the amount of data sent during the ::send process. That way, the caller method
// can get the exact number of bytes sent relative to how much data was sent. Not sure if 
// this'll be worth the effort though, albeit a minimal amount of effort.
NetPlatform::Error NetPlatform::sendtoSocket(NetSocket socket, const unsigned char *buffer, int bufferSize)
{
	if(socket == InvalidSocket)
	{
		Console->printf(ColorCode::Error, "sendToSocket: Invalid socket specified!");
		return NetPlatform::NotASocket;
	}

	// When data is sent, sends return value is equal to the amount of bytes sent.
	// When an error occurs, the return value is instead SOCKET_ERROR, and the
	// exact error code can be obtained through getLastError()
	// My initial assumption was that the return value is directly an error number
	// which explains the phenomenom I was experiencing with return values.
	int error = ::send(socket, (const char*)buffer, bufferSize, 0);

	if(error != SOCKET_ERROR ) 
		return Success;

	if(error != SOCKET_ERROR && error < bufferSize)
		Console->printf(Networking, "sendToSocket: Only %d bytes sent of %d byte buffer\r\n", error, bufferSize);
	else
		Console->printf(Networking, "sendToSocket: Socket error when attempting to send data\r\n");

	return getLastError();
}

NetSocket NetPlatform::openSocket()
{
   SOCKET retSocket;
   retSocket = socket(AF_INET, SOCK_STREAM, 0);

   if(retSocket == INVALID_SOCKET)
      return InvalidSocket;
   else
      return retSocket;
}

NetPlatform::Error NetPlatform::closeSocket(NetSocket socket)
{
	if(socket != InvalidSocket)
	{
		if(!closesocket(socket))
			return Success;
		else
			return getLastError();
	}

	return NotASocket;
}

NetPlatform::Error NetPlatform::getLastError()
{
	int err = WSAGetLastError();

	switch(err)
	{
		case 0: // If we're error checking but theres no actual error, I guess we can just assume success
			return NetPlatform::Success;
		case WSAEWOULDBLOCK:
			return NetPlatform::WouldBlock;
		case WSAEPROTOTYPE:
		case WSAEPROTONOSUPPORT:
			return NetPlatform::WrongProtocolType;
		case WSAECONNREFUSED:
			return NetPlatform::ConnectionRefused;
		case WSAETIMEDOUT:
			return NetPlatform::ConnectionTimedOut;
		case WSAECONNRESET:
			return NetPlatform::ConnectionReset;
		case WSAEBADF:
			return NetPlatform::NotASocket;
		case WSAEFAULT:
		case WSAEADDRNOTAVAIL:
			return NetPlatform::BadAddress;
		case WSAENOTCONN:
			Console->printf(ColorCode::Error, "WSA Error: Not connected\r\n");
			return NetPlatform::NotConnected;
		default:
			Console->printf(Networking, "Unhandled WSA error: %d\r\n", err);
			return NetPlatform::UnknownError;
	}
}

std::string NetPlatform::NetAddressToString(const NetAddress *address)
{
	std::string out;

	if(address->type == NetAddress::IPAddress)
	{
		std::ostringstream stream;

		stream << (int)address->netNum[0] << '.' << (int)address->netNum[1] << '.' << (int)address->netNum[2] << '.' << (int)address->netNum[3];

		out = stream.str();
	}

	return out;
}

NetPlatform::Error NetPlatform::send(NetSocket socket, const byte *buffer, int bufferSize)
{
   int error = ::send(socket, (const char*)buffer, bufferSize, 0);
   if(!error)
      return Success;
   return getLastError();
}

NetPlatform::Error NetPlatform::recv(NetSocket socket, byte *buffer, int bufferSize, int *bytesRead)
{
   *bytesRead = ::recv(socket, (char*)buffer, bufferSize, 0);
   if(*bytesRead == SOCKET_ERROR)
      return getLastError();
   return Success;
}

NetPlatform::Error NetPlatform::connect(NetSocket socket, const NetAddress *address)
{
	return Success;
}

NetPlatform::Error NetPlatform::listen(NetSocket socket, int maxConcurrentListens)
{
   if(!::listen(socket, maxConcurrentListens))
      return Success;
   return getLastError();
}

NetSocket NetPlatform::accept(NetSocket acceptSocket, NetAddress *remoteAddress)
{
   SOCKADDR_IN socketAddress;
   int addrLen = sizeof(socketAddress);

   SOCKET retVal = ::accept(acceptSocket, (PSOCKADDR) &socketAddress, &addrLen);
   if(retVal != INVALID_SOCKET)
   {
	   remoteAddress->type = NetAddress::IPAddress;
	   remoteAddress->port = htons(socketAddress.sin_port);
	   remoteAddress->netNum[0] = socketAddress.sin_addr.s_net;
	   remoteAddress->netNum[1] = socketAddress.sin_addr.s_host;
	   remoteAddress->netNum[2] = socketAddress.sin_addr.s_lh;
	   remoteAddress->netNum[3] = socketAddress.sin_addr.s_impno;
      return retVal;
   }
   return InvalidSocket;
}

NetPlatform::Error NetPlatform::detectLocalAddress(char *address)
{
#ifdef PLATFORM_WIN
	unsigned int addr_[5];
	char fullhost[255];
	u_long** a;
	struct hostent* hent;
	int num = 0;

	if( gethostname(fullhost, sizeof(fullhost)) == SOCKET_ERROR )
	{
		Console->printf(Networking, "No hostname defined!\n");
		return NetPlatform::BadAddress;
	}
	else
	{
		hent = gethostbyname(fullhost);
		if( hent == NULL ) {
			Console->printf(Networking, "Cannot resolve our own hostname to an IP address\n");
			return NetPlatform::BadAddress;
		}
		a = (u_long**)hent->h_addr_list;
		for( ; a[num] != NULL && num < 5; ++num)
			addr_[num] = (unsigned int)ntohl(*a[num]);
	}

	struct in_addr addr;
	addr.s_addr = htonl(addr_[0]);

	strncpy(address, inet_ntoa(addr), 16);
	return NetPlatform::Success;
#else
	strncpy(address, *defaultAddress, 16);
	return NetPlatform::Success;
#endif
}

NetPlatform::Error NetPlatform::bind(NetSocket socket, unsigned short port, const char *address)
{
	int error = NetPlatform::BadAddress;
	SOCKADDR_IN socketAddress;
	memset((char *)&socketAddress, 0, sizeof(socketAddress));
	socketAddress.sin_family = AF_INET;
	socketAddress.sin_port = htons(port);
	char serverAddress[16];
	
	if(address == "")
	{
		if(detectLocalAddress(serverAddress) == NetPlatform::Success)
		{
			socketAddress.sin_addr.s_addr = inet_addr( serverAddress );
			Console->printf(Networking, "Autodetecting IP to be %s\r\n", serverAddress);
		}
		else
		{
			Console->printf(Networking, "Binding to any available IP address.\r\n");
			socketAddress.sin_addr.s_addr = INADDR_ANY;
		}
	}
	else
	{
		Console->printf(Networking, "Binding to given IP: %s\r\n", address);
		socketAddress.sin_addr.s_addr = inet_addr( address );
	}

	error = ::bind(socket, (PSOCKADDR) &socketAddress, sizeof(socketAddress));

   if(error != -1)
		return Success;
   return getLastError();
}

NetPlatform::Error NetPlatform::setBufferSize(NetSocket socket, int bufferSize)
{
	return Success;
}

NetPlatform::Error NetPlatform::setBroadcast(NetSocket socket, bool broadcastEnable)
{
	return Success;
}

NetPlatform::Error NetPlatform::setBlocking(NetSocket socket, bool blockingIO)
{
#ifdef PLATFORM_WIN
	DWORD notblock = !blockingIO;
#else
	int notblock = !blockingIO;
#endif
   int error = ioctlsocket(socket, FIONBIO, &notblock);
   if(!error)
      return Success;
   return getLastError();
}