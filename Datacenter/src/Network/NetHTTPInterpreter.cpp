#include "NetHTTPInterpreter.h"
#include "NetPlatform.h"
#include "NetPacketManager.h"
#include <Common/winConsole.h>
#include <Common/SHA1.h>
#include <Common/Base64.h>
#include <sstream>
#include <regex>

NetHTTPInterpreter::NetHTTPInterpreter(NetPacket *packet, NetSession *session)
{
	httpPacket = packet;
	httpSession = session;
}

NetHTTPInterpreter::~NetHTTPInterpreter()
{

}

void NetHTTPInterpreter::InterpretPacket()
{
	const unsigned char *data; // This holds static data
	std::stringstream buffer; // This is an intermediary buffer that can easily be written to, unlike a string

	buffer << httpPacket->PacketData.getBuffer();
	std::string stringdata = buffer.str();
	data = reinterpret_cast<const unsigned char*>(stringdata.c_str());

	unsigned int dataStart = 2;
	unsigned int payloadSize = 0;

	unsigned char opcode = (unsigned char)data[0] & 0x0F;
	unsigned char fin = (data[0] >> 7) & 0x01;
	unsigned char masked = (bool)((data[1] >> 7) & 0x01);

	unsigned int length = (unsigned char)(data[1] & (~0x80));

	if(length <= 125) {
			payloadSize = length;
	}
	else if(length == 126) {
			payloadSize = (unsigned char)(data[2] + (data[3]<<8));
			dataStart += 2;
	}
	else if(length == 127) {
			payloadSize = (unsigned char)(data[2] + (data[3]<<8));
			dataStart += 8;
	}

	// This becomes our writable buffer, for unmasking purposes
	unsigned char *payload = new unsigned char[payloadSize+1]; // +1 to make room for null termination

	if(masked) 
	{
		unsigned int mask = *((unsigned int*)(data+dataStart));
		dataStart += 4;

		memcpy(payload, data+dataStart, payloadSize);

		unsigned char* c = payload;

		for(unsigned int i = 0; i < payloadSize; i++)
			c[i] = c[i] ^ ((unsigned char*)(&mask))[i%4];
	}
	else
	{
		memcpy(payload, data+dataStart, payloadSize);
	}

	payload[payloadSize] = 0; // Null terminate!

	switch(opcode)
	{
	case TextFrame:
		{
			Console->printf(Networking, "Received text frame: %s\n", payload);

			std::string reply = "Hello from server";

			NetPacket *packet = new NetPacket;

			packet->PacketData.write((unsigned char*)reply.data(), reply.size());
			packet->setPacketSize(reply.size());

			EncodeAsFrame(packet, TextFrame);

			Network->sendtoSocket(httpSession->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());

			delete packet;
		}
		break;
	case BinaryFrame:
		{
			/** This is the server parsing the received binary packet.
				The packets payload gets rewritten with the parsed payload + size,
				and then processed using our known packet structures.
			**/
			Console->printf(Networking, "Received binary frame\n");

			NetPacket *packet = new NetPacket;

			packet->PacketData.write(payload, payloadSize);

			// Re-process the packet using TCP-style buffers
			PacketManager->processPacketReceivedEvent(packet, httpSession);

			delete packet;

			/** This is how we can forge a packet to be sent to a WebSocket client.
				We initialize a packet structure, encode it using our utility function,
				and then sent it like we do a normal packet.
			**/
			std::string reply = "Hello from server";

			PacketClientMessage *message = new PacketClientMessage(reply.data(), reply.size());

			EncodeAsFrame(message, BinaryFrame);

			Network->sendtoSocket(httpSession->getSocket(), message->getPacketBuffer(), message->getPacketSize());

			delete message;
		}
		break;
	case CloseFrame:
		{
			// According to RFC6455, the server MUST reply to close frames as quickly as possible
			// The first 2 bytes received are, in network byte order, the code used to describe why the close frame
			// was sent. If there are more bytes after the first 2, then the following bytes describe the 'reason'
			// When replying, it is customary to reply with the error code that was received, if there was one
			Console->printf(Networking, "Received close frame\n");

			NetPacket *packet = new NetPacket;

			packet->setPacketSize(0);			

			if(payloadSize >= 2)
			{
				union {
					unsigned short i;
					char c[2];
				} val;

				val.c[0] = payload[0];
				val.c[1] = payload[1];

				unsigned short code = ::ntohs(val.i);

				Console->printf(Networking, "Closing code: %d\n", code);

				packet->PacketData << val.i;
				packet->setPacketSize(2);

				if(payloadSize > 2)
				{
					std::stringstream sbuffer;
					sbuffer << payload;

					Console->printf(Networking, "Closing reason: %s\n", sbuffer.str().substr(2).c_str());
				}
			}

			EncodeAsFrame(packet, CloseFrame);

			Network->sendtoSocket(httpSession->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());
		
			delete packet;
		}
		break;
	case PingFrame:
		Console->printf(Networking, "Received ping frame\n");
		break;
	case PongFrame:
		Console->printf(Networking, "Received pong frame\n");
		break;
	}

	delete[] payload;
}

//  Pattern for http headers
//  (.*[^\r\n]+):\\s(.*)\\r\\n
bool NetHTTPInterpreter::InterpretHeaders()
{
	std::stringstream buffer;
	buffer << httpPacket->PacketData.getBuffer();
	std::string input = buffer.str();
	std::regex pattern("(.*[^\r\n]+):\\s(.*)\\r\\n");
	std::smatch match;

	if(!std::regex_search(input, match, pattern))
		return false;
	
	Console->printf(Networking, "Headers detected\n");
	
	const std::sregex_iterator End;

	for (std::sregex_iterator i(input.begin(), input.end(), pattern); i != End; ++i)
	{
		Headers[(*i)[1].str()] = (*i)[2].str();
	}

	return true;
}

void NetHTTPInterpreter::AuthenticateHandshake()
{
	Console->printf(Networking, "Attempting WebSocket handshake\n");

	if(InterpretHeaders() == false)
	{
		Console->printf(Networking, "Failed to authenticate\n");
		return;
	}

	std::string accept = CalcAcceptKey();

	std::string response = "HTTP/1.1 101 Switching Protocols\r\n"
						"Upgrade: websocket\r\n"
						"Connection: Upgrade\r\n"
						"Sec-WebSocket-Accept: " + accept + "\r\n\r\n";

	Console->printf(Networking, "Sending authentication response\n");

	if(Network->sendtoSocket(httpSession->getSocket(), reinterpret_cast<const unsigned char*>(response.c_str()), response.length()) == NetPlatform::Success)
	{
		Console->printf(Networking, "Handshake authenticated!\n");
		httpSession->HandshakeComplete = true;
	} else
		Console->printf(Networking, "Failed to authenticate handshake\n");
}

std::string NetHTTPInterpreter::CalcAcceptKey()
{
	unsigned char hash[40] = {0};
	std::string input = Headers["Sec-WebSocket-Key"].data();
	input.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11"); // Magic string

	sha1::calc(input.data(), input.size(), hash);

	return base64_encode(reinterpret_cast<const unsigned char*>(hash), 20); // Hash should be of length 20
}

void NetHTTPInterpreter::EncodeAsFrame(NetPacket *packet, WebSocketOpcode opcode)
{
	const unsigned char *buffer = packet->getPacketBuffer();
	unsigned char header[16];
	int pos = 0;
	unsigned int size = packet->getPacketSize();

	header[pos++] = (unsigned char)(0x80 + opcode);

	if(size <= 125)
		header[pos++] = size;
	else if(size <= 65535)
	{
		header[pos++] = 126;
		header[pos++] = (size >> 8) & 0xFF;
		header[pos++] = size & 0xFF;
	}
	else
	{
		// Not fully supported yet
		header[pos++] = 127;
		pos += 8;
	}

	unsigned int packetSize = pos + size;
	unsigned char *data = new unsigned char[packetSize];
	memcpy(data, header, pos);
	memcpy(data+pos, buffer, size);

	packet->PacketData.clear();
	packet->PacketData.write(data, packetSize);
	packet->setPacketSize(packetSize); // Update packet size
}

bool NetHTTPInterpreter::isHTTPPacket(NetPacket *packet)
{
	std::smatch match;
	std::stringstream buffer;

	buffer << packet->PacketData.getBuffer();

	if (std::regex_search(buffer.str(), match, std::regex("^GET")))
		return true;
	
	return false;
}