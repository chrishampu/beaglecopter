#ifndef _NETPACKETMANAGER_H_
#define _NETPACKETMANAGER_H_

#include "NetPacket.h"
#include "NetPacketList.h"
#include "NetSession.h"

#include <map>
#include <boost/signals2.hpp>

typedef void (*PacketHandle)(NetStream& b, NetSession* sess);

class PacketControl
{
private:
	typedef boost::signals2::signal<void (NetPacket* packet)>  packet_signal_t;
	packet_signal_t packet_signal;

public:
    PacketControl();
    ~PacketControl();

    PacketControl(unsigned short id, PacketHandle handle);

    void processPacketReceivedEvent(NetPacket *packet, NetSession *session);

	boost::signals2::connection connectPacketReceiver(const packet_signal_t::slot_type &receiver);

	void sendPacketSignal(NetPacket &packet);

    void Register(unsigned short id, PacketHandle handle);

    void Create();
    void Destroy();
};

extern PacketControl* PacketManager;

#endif