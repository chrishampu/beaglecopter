#pragma once

#include <Common/Platform.h>
//#include "Windows/winPlatform.h"
#include "Network/NetSocket.h"
#include "Network/NetSessionManager.h"

class NetPlatform
{
public:
	NetPlatform() { }
	~NetPlatform() { }

	enum Error
	{
		Success,
		WrongProtocolType,
		InvalidPacketProtocol,
		WouldBlock,
		NotASocket,
		ConnectionRefused,
		ConnectionTimedOut,
		ConnectionReset,
		BadAddress,
		NotConnected,
		UnknownError
	};

	enum ConnectionState
	{
		DNSResolved,
		DNSFailed,
		Connected,
		ConnectFailed,
		Disconnected	
	};

	enum Protocol
	{
		UDP,
		TCP
	};

	bool Init();
	void Shutdown();

	void Process();

#ifdef PLATFORM_WIN
	void RegisterWindow();
#endif

	// TCP functions
	NetSocket openListenPort(const char *address = "", unsigned short port = 0);
	NetSocket openConnectTo(char *stringAddress, unsigned short port = 0);
	void closeConnectTo(NetSocket socket);
	Error sendtoSocket(NetSocket socket, const unsigned char *buffer, int bufferSize);

	NetSocket openSocket();
	Error closeSocket(NetSocket socket);

	static Error getLastError();
	static std::string NetPlatform::NetAddressToString(const NetAddress *address);

	// Lower level functions
	static Error send(NetSocket socket, const byte *buffer, int bufferSize);
	static Error recv(NetSocket socket, byte *buffer, int bufferSize, int *bytesRead);

	static Error connect(NetSocket socket, const NetAddress *address);
	static Error listen(NetSocket socket, int maxConcurrentListens);
	static NetSocket accept(NetSocket acceptSocket, NetAddress *remoteAddress);

	static Error detectLocalAddress(char *address);
	static Error bind(NetSocket socket, unsigned short port, const char *address = "");
	static Error setBufferSize(NetSocket socket, int bufferSize);
	static Error setBroadcast(NetSocket socket, bool broadcastEnable);
	static Error setBlocking(NetSocket socket, bool blockingIO);
};

extern NetPlatform *Network;
