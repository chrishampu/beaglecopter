#include <Common/Platform.h>
#include <Common/winConsole.h>
#include <Common/Base64.h>
#include <Network/NetPlatform.h>
#include <Network/NetPacketManager.h>
#include <Network/NetHTTPInterpreter.h>
#include <Imaging/ImageController.h>
#include <WebClient/WebClient.h>
#include <WebClient/Navigation.h>
#include <iostream>
#include <fstream>
#include <string>

AddConsoleFunction(sendimage, 1, 1)
{
	NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

	// We need at least 1 session to send to
	if (sessions.size() == 0)
		return;

	// Retrieve the first session, so we can "fake" the packet coming from it
	NetSessionManager::SessionMap::iterator first = sessions.begin();
	
	// Load up a local file to use as sending material
	std::ifstream file("AeroLogo.bmp", std::ios::in | std::ios::binary | std::ios::ate);

	if (file.is_open())
	{
		// Get the file size and create a buffer
		unsigned int size = (unsigned int)file.tellg(); // Quick n easy
		char *data = new char[size];

		file.seekg(0, std::ios::beg); // Return to beginning
		file.read(data, size); // Read the data
		file.close(); // Close it since we no longer need it

		// Create a new packet with the data
		PacketImageTransmit *imgpacket = new PacketImageTransmit(data, size);

		// Now pretend this is an incoming packet and process it
		PacketManager->processPacketReceivedEvent(imgpacket, first->second);

		// Clean up memory
		delete[] data;
		delete imgpacket;
	}
}

int main()
{
	Console->Create();
	Console->Enable(true);

	Network = new NetPlatform();
	Network->Init();

	int server = Network->openListenPort("127.0.0.1", 24000);

	ImageController *Imaging = new ImageController();
	WebClient *client = new WebClient();
	NavigationManager = new Navigation();

	while(1)
	{
		Console->Process();
		Network->Process();

		Imaging->Update();
	}

	Network->Shutdown();

	safe_delete(Network);
	safe_delete(NavigationManager);
	safe_delete(Imaging);
	safe_delete(client);

	return 0;
}