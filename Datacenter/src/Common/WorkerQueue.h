#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

template <typename T>
class WorkerQueue
{
public:

	T pop()
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		while (queue_.empty())
		{
			cond_.wait(mlock);
		}
		auto val = queue_.front();
		queue_.pop();
		return val;
	}

	void pop(T& item)
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		while (queue_.empty())
		{
			cond_.wait(mlock);
		}
		item = queue_.front();
		queue_.pop();
	}

	void push(const T& item)
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		queue_.push(item);
		mlock.unlock();
		cond_.notify_one();
	}

	int size()
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		int size = queue_.size();
		mlock.unlock();
		return size;
	}

	WorkerQueue() = default;
	WorkerQueue(const WorkerQueue&) = delete;
	WorkerQueue& operator=(const WorkerQueue&) = delete;

private:
	std::queue<T> queue_;
	std::mutex mutex_;
	std::condition_variable cond_;
};
