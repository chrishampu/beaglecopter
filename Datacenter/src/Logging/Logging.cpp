#include <Common/Platform.h>
#include "Logging.h"

#include <stdarg.h>
#include <time.h>

void Log(const char* fmt, ...)
{
#ifdef _DEBUG
	FILE* fp = NULL;
	errno_t err;

	if((err = fopen_s(&fp, "NetworkLog.log", "a")) != 0)
		return;

	va_list ap;
	va_start(ap, fmt);

	char prefix[80];
	time_t t = time(NULL);
	tm time;
	localtime_s(&time, &t);
	strftime(prefix, ARRAYSIZE(prefix), "[%H:%M:%S] Received data -\n", &time);

	fprintf(fp, "%s", prefix);
	vfprintf(fp, fmt, ap);

	fprintf(fp, "\n");

	va_end(ap);

	fclose(fp);
#endif
}

// Unoptimized due to how it loops an fprintf() function.. but oh well. Gets the job done well.
void BinLog(const unsigned char* data, size_t length)
{
#ifdef _DEBUG
	FILE* fp = NULL;
	errno_t err;

	if((err = fopen_s(&fp, "NetworkLog.log", "a")) != 0)
		return;

	char timestr[80];
	time_t t = time(NULL);
	tm time;
	localtime_s(&time, &t);
	strftime(timestr, ARRAYSIZE(timestr), "%H:%M:%S", &time);

	fprintf(fp, "[%s] Received data -\n", timestr);

	for( size_t i = 0; i < length; ++i)
		fprintf(fp, "%2.2x ", data[i]);

	fprintf(fp, "\n");
	fclose(fp);
#endif
}