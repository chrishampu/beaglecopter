#ifndef _LOGGING_H_
#define _LOGGING_H_

void Log(const char* fmt, ...);
void BinLog(const unsigned char* data, size_t length);

#endif