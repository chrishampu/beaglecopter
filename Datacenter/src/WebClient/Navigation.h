#ifndef _NAVIGATION_H_
#define _NAVIGATION_H_

#include <Network/NetPacket.h>
#include <boost/signals2.hpp>
#include <map>
#include <string>

/* Waypoints are part of the actual planes navigation, while
   Markers are a way to display important events or targets on the 
   map such as images of targets found. When an image is received
   a target is identified, a marker is created with a unique ID
   for the target, and a data structure specific to that
   target with identifying features.
  */

struct LatLng
{
	float Lat;
	float Long;
};

struct Waypoint
{
	Waypoint(unsigned int identifier, LatLng coords);

	unsigned int id;
	LatLng Coordinates;
};

struct Marker : Waypoint
{
	Marker();
	// Image data
	
	// Identifying tags
	std::string shape;
	std::string innerCharacter;
	std::string shapeColour;
	std::string innerColour;
};

class Navigation
{
public:
	Navigation();
	~Navigation();

	void Update();
	void writeMissionData(std::string drive);

	void packetReceivedEvent(NetPacket *packet);

	Waypoint *getWaypoint(unsigned int id);
	Marker *getMarker(unsigned int id);
	 
	void insertWaypoint(unsigned int id, Waypoint *point);
	void insertMarker(unsigned int id, Marker *marker);

protected:
	typedef std::map<unsigned int, Waypoint*> WaypointMap_T;
	WaypointMap_T getAllWaypoints() { return Waypoints; }

	typedef std::map<unsigned int, Marker*> MarkerMap_T;
	MarkerMap_T getAllMarkers() { return Markers; }

private:
	WaypointMap_T Waypoints;
	MarkerMap_T Markers;
	unsigned int CurrentWaypoint;
	boost::signals2::connection waypoint_connection;
};

extern Navigation *NavigationManager;

#endif