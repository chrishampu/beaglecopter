#include "WebClient.h"
#include <Network/NetPacketManager.h>
#include <Common/winConsole.h>

WebClient::WebClient()
{
	client_connection = PacketManager->connectPacketReceiver(boost::bind(&WebClient::PacketReceived, this, _1));
}

WebClient::~WebClient()
{
	client_connection.disconnect();
}

void WebClient::PacketReceived(NetPacket *packet)
{
	if(packet->GetPacketHeader() != pktClientMessage)
		return;

//  We need to cast the generic NetPacket to the specific structure we want
	PacketClientMessage *clientpacket = reinterpret_cast<PacketClientMessage*>(packet);

//  Another form, using basic casting
//	*((PacketClientMessage*)packet)->getMsg()

	Console->printf(Debug, "WebClient sent message: %s\n", clientpacket->getMsg());
}

