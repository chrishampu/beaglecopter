#include "Navigation.h"
#include <Network/NetPacketManager.h>
#include <Network/NetPacket.h>
#include <iostream>
#include <fstream>

Navigation *NavigationManager = NULL;

Navigation::Navigation()
{
	CurrentWaypoint = 0;
	waypoint_connection = PacketManager->connectPacketReceiver(boost::bind(&Navigation::packetReceivedEvent, this, _1));
}

Navigation::~Navigation()
{
	for (WaypointMap_T::iterator itr = Waypoints.begin(); itr != Waypoints.end(); itr++)
		delete itr->second;

	for (MarkerMap_T::iterator itr = Markers.begin(); itr != Markers.end(); itr++)
		delete itr->second;

	waypoint_connection.disconnect();
}

void Navigation::Update()
{
	// Here we can adjust the planes flight plan when it reaches a 
	// waypoint and must go to the next one. Or anything else that needs to be
	// done in real time.
}

void Navigation::writeMissionData(std::string drive)
{
	drive.append("/AERO.txt");

	std::ofstream file(drive, std::ios::out);

	for (MarkerMap_T::iterator itr = Markers.begin(); itr != Markers.end(); itr++)
	{
		Marker *marker = itr->second;
		file << marker->id << "\t" << marker->Coordinates.Lat << " " << marker->Coordinates.Long << std::endl;
	}

	file.close();
}

void Navigation::packetReceivedEvent(NetPacket *packet)
{
	switch (packet->GetPacketHeader())
	{
		case pktWaypointUpdate:
			break;
		case pktMarkerUpdate:
			break;
	}
}

Waypoint *Navigation::getWaypoint(unsigned int id)
{
	return Waypoints[id];
}

Marker *Navigation::getMarker(unsigned int id)
{
	return Markers[id];
}

void Navigation::insertWaypoint(unsigned int id, Waypoint *point)
{
	Waypoints[id] = point;
}

void Navigation::insertMarker(unsigned int id, Marker *marker)
{
	Markers[id] = marker;
}
