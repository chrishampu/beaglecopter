#ifndef _WEBCLIENT_H_
#define _WEBCLIENT_H_

#include <boost\signals2.hpp>
#include <thread>

class NetPacket;

class WebClient
{
public:
	WebClient();
	~WebClient();

	void PacketReceived(NetPacket *packet);

	void Main();

	void Update();

private:
	std::thread WebClientThread;
	boost::signals2::connection client_connection;
};

#endif
