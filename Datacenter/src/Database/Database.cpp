#include <Common/Platform.h>
#include "Database.h"
//#include "Windows/winConsole.h"

static int MaxConnections = 2;

Database::Database(std::string host, std::string user, std::string pass, std::string name, int port)
	: Hostname(host),
	  Username(user),
	  Password(pass),
	  DatabaseName(name),
	  Port(port)
{
	Initialize();
}

Database::~Database()
{
	Shutdown();
}

bool Database::Initialize()
{
	for(int i = 0; i < MaxConnections; i++)
	{
		Connection *con = new Connection;

		con->Open(Hostname, Username, Password, DatabaseName, Port);

		AddConnection(con);
	}

	//Console->printf(MySQL, "%d '%s' database connections prepared\n", MaxConnections, DatabaseName.c_str());

	return true;
}

void Database::Shutdown()
{
	ConnectionIter iter;

	for(iter = Connections.begin(); iter != Connections.end();)
	{
		((Connection*)*iter)->Close();
		delete *iter;
		iter = Connections.erase(iter);
	}
}

void Database::AddConnection(Connection *con)
{
	Connections.push_back(con);
}

Connection* Database::GetFreeConnection()
{
	Connection *con = NULL;
	ConnectionIter iter;

	for(;;)
	{
		for(iter = Connections.begin(); iter != Connections.end();)
		{
			con = *iter;

			if(con->TryLock())
				return con;
		}
	}

	return NULL;
}

QueryResult* Database::Query(const char* sql)
{
	Connection *con = GetFreeConnection();
	QueryResult *res = NULL;

	if(con->isValid() == true)
	{
		if( SendQuery(con, sql) )
			res = StoreQueryResult(con);
	}

	con->Unlock();
	
	return res;
}

void Database::Execute(const char* sql)
{
	Connection *con = GetFreeConnection();

	if(con->isValid() == true)
		SendQuery(con, sql);

	con->Unlock();
}

std::string Database::EscapeString(std::string Escape)
{
	char a2[16384] = { 0 };

	Connection* con = GetFreeConnection();
	std::string ret;

	if(mysql_real_escape_string(con->get(), a2, Escape.c_str(), (unsigned long)Escape.length()) == 0)
		ret = Escape.c_str();
	else
		ret = a2;

	con->Unlock();

	return std::string(ret);
}

void Database::KeepAlive()
{
	ConnectionIter iter;

	for(iter = Connections.begin(); iter != Connections.end();)
	{
		Connection *con = *iter;
		if(con->TryLock())
		{
			con->Ping();
			con->Unlock();
		}
	}

	//Console->printf(MySQL, "Keeping connections alive\n");
}

bool Database::SendQuery(Connection *con, const char* sql)
{
	int result = mysql_query(con->get(), sql);

//	if(result > 0 )
//		Console->printf(MySQL, "Query exception: '%s'\nFailed query: '%s'\n", mysql_error(con->get()), sql);

	return (result == 0 ? true : false);
}

QueryResult* Database::StoreQueryResult(Connection *con)
{
	QueryResult* res;

	MYSQL_RES* pRes = mysql_store_result(con->get());
	unsigned int uRows = (unsigned int)mysql_affected_rows(con->get());
	unsigned int uFields = (unsigned int)mysql_field_count(con->get());

	if(uRows == 0 || uFields == 0 || pRes == 0)
	{
		if(pRes != NULL)
			mysql_free_result(pRes);

		return NULL;
	}

	res = new QueryResult(pRes, uFields, uRows);
	res->NextRow();

	return res;
}

/*
Connection* Database::operator[](std::string Database)
{
	ConnectionConstIter iter = Connections.find(Database);

	if(iter != Connections.end())
		return Connections[Database];
	else
		return NULL;
}
*/