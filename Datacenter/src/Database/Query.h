#pragma once

#include <Common/Platform.h>
#include <mysql/mysql.h>

class Field
{
public:
	void SetValue(char* value) { Value = value; }

	const char* GetString() { return Value; }

	float GetFloat() { return Value ? static_cast<float>(atof(Value)) : 0; }

	bool GetBool() { return Value ? atoi(Value) > 0 : false; }

	unsigned char GetUChar() { return Value ? static_cast<unsigned char>(atol(Value)) : 0; }
	char GetChar() { return Value ? static_cast<char>(atol(Value)) : 0; }

	unsigned short GetUShort() { return Value ? static_cast<unsigned short>(atol(Value)) : 0; }
	short GetShort() { return Value ? static_cast<short>(atol(Value)) : 0; }

	unsigned int GetUInt() { return Value ? static_cast<unsigned int>(atol(Value)) : 0; }
	int GetInt() { return Value ? static_cast<int>(atol(Value)) : 0; }

private:
	char* Value;
};

class QueryResult
{
public:
	QueryResult(MYSQL_RES* res, unsigned int Fields, unsigned int Rows);
	~QueryResult();

	void Release();

	bool NextRow();

	Field* Fetch() { return CurrentRow; }
	unsigned int GetFieldCount() const { return FieldCount; }
	unsigned int GetRowCount() const { return RowCount; }

	const Field & operator [] (unsigned int index) const
	{
		return CurrentRow[index];
	}

protected:
	unsigned int FieldCount;
	unsigned int RowCount;
	Field* CurrentRow;
	MYSQL_RES* Result;
};