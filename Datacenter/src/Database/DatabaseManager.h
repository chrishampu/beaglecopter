#pragma once

#include "Database.h"
#include <map>

class DatabaseManager
{
public:
	DatabaseManager();
	~DatabaseManager();

	virtual void Initialize() = 0;
	void Shutdown();

	void AddDatabase(int index, Database *db);
	void RemoveDatabase(int index);

	void KeepAlive();

	Database* operator[](int index);

private:
	typedef std::map<int, Database*> DatabaseMap;
	DatabaseMap mDatabases;

public:
	typedef DatabaseMap::iterator DatabaseIter;
};