#include "DatabaseManager.h"
//#include "Windows/winConsole.h"

DatabaseManager::DatabaseManager()
{
	Initialize();
}

DatabaseManager::~DatabaseManager()
{
	Shutdown();
}

void DatabaseManager::Initialize()
{

}

void DatabaseManager::Shutdown()
{
	DatabaseIter iter;

	for(iter = mDatabases.begin(); iter != mDatabases.end(); iter++)
	{
		delete iter->second;
	}

	mDatabases.clear();
}

void DatabaseManager::AddDatabase(int index, Database *db)
{
	mDatabases[index] = db;
}

void DatabaseManager::RemoveDatabase(int index)
{
	DatabaseMap::const_iterator iter = mDatabases.find(index);

	if(iter != mDatabases.end())
	{
		delete mDatabases[index];
		mDatabases.erase(index);
	}
}

void DatabaseManager::KeepAlive()
{
	DatabaseIter iter;

	for(iter = mDatabases.begin(); iter != mDatabases.end(); iter++)
		iter->second->KeepAlive();

	//Console->printf(MySQL, "Keeping connections alive\n");
}

Database* DatabaseManager::operator[](int index)
{
	DatabaseMap::const_iterator iter = mDatabases.find(index);

	if(iter != mDatabases.end())
		return mDatabases[index];
	else
		return NULL;
}

/*
Database* DatabaseManager::get(const char *name)
{
	DatabaseMap::const_iterator iter = mDatabases.find(name);

	if(iter != mDatabases.end())
		return mDatabases[name];
}
*/