#include "Query.h"

QueryResult::QueryResult(MYSQL_RES* res, unsigned int Fields, unsigned int Rows)
	: RowCount(Fields),
	  FieldCount(Rows),
	  CurrentRow(NULL),
	  Result(res)
{
	CurrentRow = new Field[FieldCount];
}

QueryResult::~QueryResult()
{
	mysql_free_result(Result);
	delete [] CurrentRow;
}

void QueryResult::Release()
{
	delete this; // Assuming that this object was constructed and allocated with new
}

bool QueryResult::NextRow()
{
	MYSQL_ROW row = mysql_fetch_row(Result);

	if(row == NULL)
		return false;

	for(unsigned int i = 0; i < FieldCount; ++i)
		CurrentRow[i].SetValue(row[i]);

	return true;
}