#include "ImageController.h"
#include <Network/NetPlatform.h>
#include <Network/NetPacketManager.h>
#include <Network/NetSessionManager.h>
#include <Network/NetHTTPInterpreter.h>
#include <Common/winConsole.h>
#include <Common/Base64.h>
#include <iostream>
#include <fstream>
#include <string>

ImageController::ImageController()
{
	ImageProcessingThread = std::thread(&ImageController::ProcessingLoop, this);
}

ImageController::~ImageController()
{
	while (ProcessingQueue.size())
	{
		PacketImageTransmit *packet = ProcessingQueue.pop();
		delete packet;
	}
}

void ImageController::notifyImageReady(NetPacket *packet)
{
	if(packet->GetPacketHeader() != pktImageTransmit)
		return;

	PacketImageTransmit *imgpacket = reinterpret_cast<PacketImageTransmit*>(packet);

	// Create a copy of the packet
	PacketImageTransmit *QueuedPacket = new PacketImageTransmit(imgpacket->getImageData(), imgpacket->getImageSize());

	ProcessingQueue.push(QueuedPacket);

	Console->printf(Networking, "Queued image of size %d for processing\n", QueuedPacket->getImageSize());
}

void ImageController::ProcessingLoop()
{
	image_connection = PacketManager->connectPacketReceiver(boost::bind(&ImageController::notifyImageReady, this, _1));

	while (true)
	{
		PacketImageTransmit *packet = ProcessingQueue.pop();

		/* Image processing for this image happens here. Example
		
			// Create a buffer of the data
			unsigned char* data = new unsigner char[packet->getImageSize()];
			memcpy(data, packet->getImageData(), packet->getImageSize());

			delete packet; // we no longer need this

			-- start processing data
			-- interpret BMP image data
			-- end procesing data

			// Now we create a packet to send to the webclients
			PacketImageTransmit *processedPacket = new PacketImageTransmit(data, sizeof(data));

			// Send processed packet
			ProcessedPackets.push(processedPacket);

			delete[] data;
			delete processedPacket;

		*/

		// For this example, we are going to assume that the received packet is the binary
		// data of a BMP image; the entire image file. This image file gets Base64 encoded
		// and encoded as a binary packet for sending to WebSocket clients.

		std::string encoded = base64_encode(reinterpret_cast<const unsigned char*>(packet->getImageData()), packet->getImageSize());

		// Base64 encode our image data
		const char *payload = encoded.c_str();

		// Create a packet
		PacketImageTransmit *processedPacket = new PacketImageTransmit(payload, encoded.size());

		// Encode it as a WebSocket Binary packet
		NetHTTPInterpreter::EncodeAsFrame(processedPacket, BinaryFrame);

		// Add it to our queue of processed packets
		ProcessedPackets.push(processedPacket);
		
		// Clean up the packet we orginally pulled from the queue
		delete packet;
	}

	image_connection.disconnect();
}

void ImageController::Update()
{
	while (ProcessedPackets.size())
	{
		PacketImageTransmit *packet = ProcessedPackets.pop();

		// Here we pull all of the available sessions
		NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

		// And iterate through all of the sessions
		for (NetSessionManager::SessionMap::iterator itr = sessions.begin(), next; itr != sessions.end(); itr = next)
		{
			next = itr;
			++next;
			
			// Send to all WebSocket sessions
			if (itr->second->getSocketType() == WebSocket)
				Network->sendtoSocket(itr->second->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());
		}
		
		delete packet;
	}
}