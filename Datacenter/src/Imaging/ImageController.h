#ifndef _IMAGE_CONTROLLER_H_
#define _IMAGE_CONTROLLER_H_

#include <Network/Packets/PacketImageTransmit.h>
#include <Common/WorkerQueue.h>
#include <boost\signals2.hpp>
#include <thread>

class ImageController
{
public:
	ImageController();
	~ImageController();

	void notifyImageReady(NetPacket *packet);

	void ProcessingLoop();
	void Update();

private:
	boost::signals2::connection image_connection;
	std::thread ImageProcessingThread;
	WorkerQueue<PacketImageTransmit*> ProcessingQueue;
	WorkerQueue<PacketImageTransmit*> ProcessedPackets;
};

#endif