#include "Motor.h"
#include <unistd.h>

MotorControl *MotorController = NULL;

#define MotorCount 4

Motor::Motor(Motors motor)
{
	std::string file = MotorData[motor].file;
	file.append( "/duty_ns" );
	duty_ns.open(file.c_str(), std::ios::in|std::ios::out);

	file = MotorData[motor].file;
	file.append( "/run" );
	run.open(file.c_str(), std::ios::in|std::ios::out);
	
	if(run.is_open() == false)
		puts("Run stream is not open!");
	
	file = MotorData[motor].file;
	file.append( "/period_freq" );
	freq.open(file.c_str(), std::ios::in|std::ios::out);
	
	file = MotorData[motor].file;
	file.append( "/request" );
	req.open(file.c_str(), std::ios::in|std::ios::out);
	
	requested = false;
	running = false;
	armed = false;
	id = motor;
}

Motor::~Motor()
{
	Stop();
	UnRequest();
}

void Motor::setFrequency(short freq)
{
	this->freq << freq << std::endl;
	frequency = freq;
}

void Motor::setSpeed(short speed)
{
	int s = 1000000 + speed * 10000;
	duty_ns << s << std::endl;
	duty = s;
}

int Motor::getSpeed()
{
	return (duty - 1000000) / 10000;
}

void Motor::Request()
{
	if(isRequested() == true)
		return;
		
	req << 1 << std::endl;
	requested = true;
}

void Motor::UnRequest()
{
	if(isRequested() == false)
		return;
		
	req << 0 << std::endl;
	requested = false;
}

// Arming will be blocking until a proper loop/polling is setup, due to sleep()
void Motor::Arm()
{
	if(armed == true)
		return;
	
	puts("Arming motor");
	
	armed = true;
	running = false;
	
	setSpeed( 0 );
	Run();
	
	usleep( 3000 * 1000 ); // 3 seconds until all beeps are finished
	
	puts("Motor hot");
}

void Motor::Run()
{
	if(armed == false)
		return;
		
	if(running == true)
		return;

	run << 1 << std::endl;
	running = true;
}

void Motor::Stop()
{
	if(armed == false)
		return;
		
	if(running == false)
		return;

	setSpeed( 0 );
		
	run << 0 << std::endl;
	running = false;
}

bool Motor::isRequested()
{
	return requested;
}

bool Motor::isArmed()
{
	return armed;
}

MotorControl::MotorControl()
{

}

MotorControl::~MotorControl()
{
	for(int i = 0; i < MotorCount; i++)
	{
		delete mMotor[i];
	}
}

void MotorControl::setFrequency(Motors m, short freq)
{
	mMotor[m]->setFrequency(freq);
}

void MotorControl::setFrequency(short freq)
{
	for(int i = 0; i < MotorCount; i++)
	{
		mMotor[i]->setFrequency(freq);
	}
}
	
void MotorControl::setSpeed(Motors m, short speed)
{
	mMotor[m]->setSpeed(speed);
}

void MotorControl::setSpeed(short speed)
{
	for(int i = 0; i < MotorCount; i++)
	{
		mMotor[i]->setSpeed(speed);
	}	
}
	
void MotorControl::SetupMuxing()
{
	/*
	std::ofstream pwm00("/sys/kernel/debug/omap_mux/mcasp0_aclkx");
	std::ofstream pwm01("/sys/kernel/debug/omap_mux/mcasp0_fsx");
	std::ofstream pwm10("/sys/kernel/debug/omap_mux/gpmc_a2");
	std::ofstream pwm11("/sys/kernel/debug/omap_mux/gpmc_a3");
	
	pwm00 << 1 << std::endl; // P9-31 CCW
	pwm01 << 1 << std::endl; // P9-29 CCW
	pwm10 << 0x8006 << std::endl; // P9-14 CW
	pwm11 << 0x8006 << std::endl; // P9-16 CW
	*/
	
	system("echo 1 > /sys/kernel/debug/omap_mux/mcasp0_aclkx");
	system("echo 1 > /sys/kernel/debug/omap_mux/mcasp0_fsx");
	system("echo 0x8006 > /sys/kernel/debug/omap_mux/gpmc_a2");
	system("echo 0x8006 > /sys/kernel/debug/omap_mux/gpmc_a3");
	
	puts("Pins muxed");
}

void MotorControl::SetupMotors(bool useDefaults)
{
	for(int i = 0; i < MotorCount; i++)
	{
		printf("Setup of motor %d\n", (i+1));
		
		mMotor[i] = new Motor( (Motors)i );
		
		if(useDefaults == true)
		{
			if(mMotor[i]->isRequested() == false)
				mMotor[i]->Request();
			
			mMotor[i]->setFrequency( 50 );
			mMotor[i]->setSpeed( 0 );
			
			printf("Motor initialized\n");
		}
	}
}

void MotorControl::Arm(Motors m)
{
	mMotor[m]->Arm();
}

void MotorControl::Arm()
{
	for(int i = 0; i < MotorCount; i++)
	{
		mMotor[i]->Arm();
	}
}
	
void MotorControl::Run(Motors m)
{
	mMotor[m]->Run();
}

void MotorControl::Run()
{
	for(int i = 0; i < MotorCount; i++)
	{
		mMotor[i]->Run();
	}
}
	
void MotorControl::Stop(Motors m)
{
	mMotor[m]->Stop();
}

void MotorControl::Stop()
{
	for(int i = 0; i < MotorCount; i++)
	{
		mMotor[i]->Stop();
	}
}
	
bool MotorControl::isArmed(Motors m)
{
	return mMotor[m]->isArmed();
}
