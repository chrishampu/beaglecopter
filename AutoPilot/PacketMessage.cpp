#include "PacketMessage.h"

#include <stdio.h>
#include <string.h>

PacketMsg::PacketMsg()
{
	packetID = pktMsg;
	packetSize = 2;
	msglen = 0;
}

PacketMsg::PacketMsg(const char *msg, int len)
{
	//PacketData.clear();

	packetID = pktMsg;

	memset(&this->msg, 0, 256);
	memcpy(&this->msg, msg, len);
	//strcpy_s(this->msg, 256, msg);

	msglen = len;
	setPacketSize(msglen + 2 + 2); // msg size + msglen + packetID

	PreparePacket();
}

void PacketMsg::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << msglen;
	PacketData << msg;
}

void PacketMsg::Handle(NetStream& ns)
{
	Decode(ns);

	printf("Packet message: %s\r\n", msg);
}

void PacketMsg::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> msglen;

	memset(&msg, 0, 256);
	ns.read((unsigned char*)msg, msglen);
}

const char *PacketMsg::getMsg()
{
	return msg;
}

unsigned short PacketMsg::getMsgLen()
{
	return msglen;
}
