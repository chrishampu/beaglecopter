#include "Server.h"
#include "EventInterface.h"
#include "NetPacketManager.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <errno.h>
#include <net/if_ppp.h>
#include <sys/ioctl.h>   /* ioctl() */
#include <net/ppp_defs.h>
#include <stdlib.h>
#include <cstring>
#include <vector>

using namespace std;

Net *Network = NULL;

static Net::Error getLastError();

const char* serverIP = "169.254.246.15";

NetSocket clientSock = InvalidSocket;

enum SocketState
{
   InvalidState,
   Connected,
   ConnectionPending,
   Listening,
   NameLookupRequired
};

struct Socket
{
      Socket()
      {
         fd = InvalidSocket;
         state = InvalidState;
         remoteAddr[0] = 0;
         remotePort = -1;
      }

      NetSocket fd;
      int state;
      char remoteAddr[256];
      int remotePort;
};

static std::vector<Socket*> PolledSockets;

static Socket* addPolledSocket(NetSocket& fd, int state,
                               char* remoteAddr = NULL, int port = -1)
{
   Socket* sock = new Socket();
   sock->fd = fd;
   sock->state = state;
   
   if (remoteAddr)
      strcpy(sock->remoteAddr, remoteAddr);
	  
   if (port != -1)
      sock->remotePort = port;
	  
   PolledSockets.push_back(sock);
   
   return sock;
}

bool Net::init()
{
	PacketManager->Create();

	return(true);
}

void Net::shutdown()
{
   while (PolledSockets.size() > 0)
      closeConnectTo(PolledSockets[0]->fd);
	  
	PacketManager->Destroy();
}

static void netToIPSocketAddress(const NetAddress *address, struct sockaddr_in *sockAddr)
{
   memset(sockAddr, 0, sizeof(struct sockaddr_in));
   sockAddr->sin_family = AF_INET;
   sockAddr->sin_port = htons(address->port);
   char tAddr[20];
   snprintf(tAddr, 20, "%d.%d.%d.%d\n", address->netNum[0], address->netNum[1], address->netNum[2], address->netNum[3]);

   sockAddr->sin_addr.s_addr = inet_addr(tAddr);
}

static void IPSocketToNetAddress(const struct sockaddr_in *sockAddr, NetAddress *address)
{
   address->type = NetAddress::IPAddress;
   address->port = htons(sockAddr->sin_port);
   char *tAddr;
   tAddr = inet_ntoa(sockAddr->sin_addr);
   unsigned char nets[4];
   nets[0] = atoi(strtok(tAddr, "."));
   nets[1] = atoi(strtok(NULL, "."));
   nets[2] = atoi(strtok(NULL, "."));
   nets[3] = atoi(strtok(NULL, "."));

   address->netNum[0] = nets[0];
   address->netNum[1] = nets[1];
   address->netNum[2] = nets[2];
   address->netNum[3] = nets[3];
}

NetSocket Net::openListenPort(unsigned short port)
{
   NetSocket sock = openSocket();
   if (sock == InvalidSocket)
   {
      printf("Unable to open listen socket: %s", strerror(errno));
      return InvalidSocket;
   }

   if (bind(sock, port) != NoError)
   {
      printf("Unable to bind port %d: %s", port, strerror(errno));
      ::close(sock);
      return InvalidSocket;
   }
   if (listen(sock, 4) != NoError)
   {
      printf("Unable to listen on port %d: %s", port, strerror(errno));
      ::close(sock);
      return InvalidSocket;
   }

   setBlocking(sock, false);
   addPolledSocket(sock, Listening);

   return sock;
}

void Net::closeConnectTo(NetSocket sock)
{
   // if this socket is in the list of polled sockets, remove it
   for (int i = 0; i < PolledSockets.size(); ++i)
      if (PolledSockets[i]->fd == sock)
      {
         delete PolledSockets[i];
         PolledSockets.erase(PolledSockets.begin() + i);
         break;
      }
   
   closeSocket(sock);
}

void Net::process()
{
   if (PolledSockets.size() == 0)
      return;

   int optval;
   socklen_t optlen = sizeof(int);
   int bytesRead;
   Net::Error err;
   bool removeSock = false;
   Socket *currentSock = NULL;
   sockaddr_in ipAddr;
   NetSocket incoming = InvalidSocket;
   char out_h_addr[1024];
   int out_h_length = 0;
    unsigned char data[512];
	NetAddress address;
	
	static PacketReceivedEvent packetEvent;
   
   for (int i = 0; i < PolledSockets.size(); 
        /* no increment, this is done at end of loop body */)
   {
      removeSock = false;
      currentSock = PolledSockets[i];
      switch (currentSock->state)
      {
         case InvalidState:
            printf("Error, InvalidState socket in polled sockets list");
            break;
         case ConnectionPending:
			printf("Pending connection\n");

            if (getsockopt(currentSock->fd, SOL_SOCKET, SO_ERROR, 
                           &optval, &optlen) == -1)
            {
               printf("Error getting socket options: %s", strerror(errno));
               removeSock = true;
            }
            else
            {
               if (optval == EINPROGRESS)
                  // still connecting
                  break;

               if (optval == 0)
               {
                  currentSock->state = Connected;
               }
               else
               {
                  printf("Error connecting: %s", strerror(errno));
                  removeSock = true;
               }
            }
            break;
         case Connected:
            bytesRead = 0;
            // try to get some data
            err = Net::recv(currentSock->fd, packetEvent.payload, 
                            MaxPacketDataSize, &packetEvent.len);
            if(err == Net::NoError)
            {
               if (packetEvent.len > 0)
               {
					EventInterface->packEvent(packetEvent);
               }
               else 
               {
                  if (packetEvent.len < 0)
                     printf("Unexpected error on socket: %s", 
                                 strerror(errno));

                  removeSock = true;
               }
			   
			   printf("Reading data\n");
            }
            else if (err != Net::NoError && err != Net::WouldBlock)
            {
               printf("Error reading from socket: %s", strerror(errno));
               removeSock = true;
            }
            break;
         case NameLookupRequired: 
			printf("Name lookup required\n");		 
            if (out_h_length == -1)
            {
               printf("DNS lookup failed: %s", currentSock->remoteAddr);
               removeSock = true;
            }
            else
            {
               memcpy(&(ipAddr.sin_addr.s_addr), out_h_addr, out_h_length);
               ipAddr.sin_port = currentSock->remotePort;
               ipAddr.sin_family = AF_INET;
               if(::connect(currentSock->fd, (struct sockaddr *)&ipAddr, 
                            sizeof(ipAddr)) == -1)
               {
                  if (errno == EINPROGRESS)
                  {
                     currentSock->state = ConnectionPending;
                  }
                  else
                  {
                     printf("Error connecting to %s: %s", 
                                 currentSock->remoteAddr, strerror(errno));
                     removeSock = true;
                  }
               }
               else
               {
                  currentSock->state = Connected;
               }
            }		
            break;
    	 case Listening:
            incoming = 
               Net::accept(currentSock->fd, &address);
            if(incoming != InvalidSocket)
            {
               clientSock = incoming;
		setBlocking(incoming, false);
               addPolledSocket(incoming, Connected);
			   printf("Accepting connection\n");
            }
            break;
      }

      // only increment index if we're not removing the connection, since 
      // the removal will shift the indices down by one
      if (removeSock)
         closeConnectTo(currentSock->fd);
      else
         i++;
   }
}
                 
NetSocket Net::openSocket()
{
   int retSocket;
   retSocket = socket(AF_INET, SOCK_STREAM, 0);

   if(retSocket == InvalidSocket)
      return InvalidSocket;
   else
      return retSocket;
}

Net::Error Net::closeSocket(NetSocket socket)
{
   if(socket != InvalidSocket)
   {
      if(!close(socket))
         return NoError;
      else
         return getLastError();
   }
   else
      return NotASocket;
}

Net::Error Net::listen(NetSocket socket, int backlog)
{
   if(!::listen(socket, backlog))
   {
	  printf("Listening on socket\n");
      return NoError;
	}
   return getLastError();
}

NetSocket Net::accept(NetSocket acceptSocket, NetAddress *remoteAddress)
{
   sockaddr_in socketAddress;
   unsigned int addrLen = sizeof(socketAddress);
   
   int retVal = ::accept(acceptSocket, (sockaddr *) &socketAddress, &addrLen);
   if(retVal != InvalidSocket)
   {
      IPSocketToNetAddress(&socketAddress, remoteAddress);
      return retVal;
   }
   return InvalidSocket;
}

Net::Error Net::bind(NetSocket socket, unsigned short port)
{
   int error;
   
   sockaddr_in socketAddress;
   memset((char *)&socketAddress, 0, sizeof(socketAddress));
   socketAddress.sin_family = AF_INET;

   if( serverIP[0] != '\0' ) {
      // we're not empty
      socketAddress.sin_addr.s_addr = inet_addr( serverIP );

      if( socketAddress.sin_addr.s_addr != INADDR_NONE ) {
	 printf( "Binding server port to %s\n", serverIP );
      } else {
	 printf("inet_addr() failed for %s while binding!", serverIP );
	 socketAddress.sin_addr.s_addr = INADDR_ANY;
      }

   } else {
      printf( "Binding server port to default IP" );
      socketAddress.sin_addr.s_addr = INADDR_ANY;
   }

   socketAddress.sin_port = htons(port);
   error = ::bind(socket, (sockaddr *) &socketAddress, sizeof(socketAddress));

   if(!error)
      return NoError;
   return getLastError();
}

Net::Error Net::setBroadcast(NetSocket socket, bool broadcast)
{
   int bc = broadcast;
   int error = setsockopt(socket, SOL_SOCKET, SO_BROADCAST, (char*)&bc, sizeof(bc));
   if(!error)
      return NoError;
   return getLastError();
}

Net::Error Net::setBlocking(NetSocket socket, bool blockingIO)
{
   int notblock = !blockingIO;
   int error = ioctl(socket, FIONBIO, &notblock);
   if(!error)
      return NoError;
   return getLastError();   
}

Net::Error Net::recv(NetSocket socket, unsigned char *buffer, int bufferSize, int *bytesRead)
{
   *bytesRead = ::recv(socket, (char*)buffer, bufferSize, 0);
   if(*bytesRead == -1)
      return getLastError();
   return NoError;
}

Net::Error Net::send(NetSocket socket, const unsigned char *buffer, 
unsigned int bufferSize)
{
	int error = ::send(socket, buffer, bufferSize, 0);
	if(!error)
		return NoError;
	return getLastError();
}

Net::Error getLastError()
{
   if (errno == EAGAIN)
      return Net::WouldBlock;
   return Net::UnknownError;
}
