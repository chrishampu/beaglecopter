#ifndef _MOTOR_H_
#define _MOTOR_H_

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <sstream>

static enum Motors
{
	MotorA = 0, // Pin P9-31
	MotorB, // P9-29
	MotorC, // P9-14
	MotorD, // P9-16
	MaxMotors
} MotorEnum;

static struct {
	Motors motor;
	std::string file;
	bool setFreq;
} MotorData[] = {
	{ MotorA, "/sys/class/pwm/ehrpwm.0:0", true },
	{ MotorB, "/sys/class/pwm/ehrpwm.0:1", false },
	{ MotorC, "/sys/class/pwm/ehrpwm.1:0", true },
	{ MotorD, "/sys/class/pwm/ehrpwm.1:1", false }
};

class MotorControl;

class Motor
{
	friend class MotorControl;
public:
	Motor(Motors motor);
	~Motor();
	
	void setFrequency(short freq);	
	void setSpeed(short speed);
	
	int getSpeed();
	
	void Request();
	void UnRequest();
	void Arm();
	void Run();
	void Stop();
	
	bool isRequested();
	bool isArmed();
	
private:
	bool requested;
	bool armed;
	bool running;
	int frequency;
	int duty;
	Motors id;

protected:
	std::fstream duty_ns;
	std::fstream run;
	std::fstream freq;
	std::fstream req;
};

class MotorControl
{
public:
	MotorControl();
	~MotorControl();
	
	void setFrequency(Motors m, short freq);
	void setFrequency(short freq);
	
	void setSpeed(Motors m, short speed);
	void setSpeed(short speed);
	
	void SetupMuxing();
	void SetupMotors(bool useDefaults = true);
	
	void Arm(Motors m);
	void Arm();
	
	void Run(Motors m);
	void Run();
	
	void Stop(Motors m);
	void Stop();
	
	bool isArmed(Motors m);
	
private:
	Motor *mMotor[MaxMotors];
};

extern MotorControl *MotorController;

#endif