#ifndef _PACKET_MESSAGE_H_
#define _PACKET_MESSAGE_H_

#include "NetPacket.h"

class PacketMsg : public NetPacket
{
public:
    PacketMsg();
    PacketMsg(const char *msg, int len);

    virtual void PreparePacket();

    virtual void Handle(NetStream& ns);

    virtual void Decode(NetStream& ns);

    const char *getMsg();
    unsigned short getMsgLen();

private:
    char msg[256];
    unsigned short msglen;
};

#endif