#ifndef _SENSOR_H_
#define _SENSOR_H_

#include <string>

#include "Vector3.h"

#define SAMPLECOUNT 400
#define FINDZERO 49

class SensorController;

class Gyroscope
{
friend class SensorController;
public:
	Gyroscope();
	~Gyroscope();

	void Measure();
	
	void Calibrate(float &zeroX, float &zeroY, float &zeroZ);
	void Finalize(float *zeroX, float *zeroY, float *zeroZ);
	
	Vector3& getAngle() { return Angle; }
	
protected:
	Vector3 Angle;
	Vector3 RawAngle;
	Vector3 Zero;
	
	float gyroScaleFactor;
	float Heading;
};

class Accelerometer
{
friend class SensorController;
public:
	Accelerometer();
	~Accelerometer();

	void Measure();
	
	void Calibrate();
	void Finalize();
	
	Vector3& getAngle() { return Angle; }
	
protected:
	Vector3 AccelSum;
	Vector3 Bias;
	Vector3 Angle;
	Vector3 RawAngle;
	float accelOneG;
};

class Magnetometer
{
friend class SensorController;
public:
	Magnetometer();
	~Magnetometer();
	
	void Measure(float roll, float pitch);
	
	void Calibrate();
	void Finalize();
	
	Vector3& getAngle() { return Angle; }
	
protected:
	Vector3 Angle;
	Vector3 RawAngle;
	Vector3 Bias;
	float Heading;
	float HeadingX;
	float HeadingY;
};

class SensorController
{
public:
	SensorController();
	~SensorController();
	
	void SetupMuxing();
	void SetupSerial();
	
	void MeasureSensors();
	void CalibrateSensors();
	void PollSensors();
	
	int getPressure() { return Pressure; }
	float getAltitude() { return Altitude; }
	float getCalibratedAltitude() { return AltitudeCalibrated; }
	float getTemperature() { return Temperature; }
	
	Gyroscope* Gyro() { return mGyro; }
	Accelerometer* Accel() { return mAccel; }
	Magnetometer* Magnet() { return mMagnet; }

	float getPitch() { return Pitch; }
	float getYaw() { return Yaw; }
	float getRoll() { return Roll; }
	
	int processCount;
	
protected:
	void process_string(std::string str);
	
private:
	int serialHandle;

	Gyroscope *mGyro;
	Accelerometer *mAccel;
	Magnetometer *mMagnet;
	
	float Pitch;
	float Yaw;
	float Roll;
	
	int Pressure;
	float Altitude;
	float AltitudeCalibrated;
	float Temperature;
	
	bool first_run;
	std::string last_str;
};

extern SensorController *Sensors;

#endif
