﻿#include "Server.h"
#include "EventInterface.h"
#include "Motor.h"
#include "Sensor.h"
#include "Util.h"
#include "NetPacketList.h"
#include "Kinematics.h"
#include "FlightController.h"

#include <stdio.h>

int main()
{
	EventInterface->Initialize();

	Network = new Net();
	MotorController = new MotorControl();
	Sensors = new SensorController();
	Kinematics = new Stabilization();
	FlightControl = new FlightController();
	
	Network->init();
	
	NetSocket clientSock = Network->openListenPort(24000);
	
	MotorController->SetupMuxing();
	MotorController->SetupMotors(true);
	
	Sensors->SetupMuxing();
	Sensors->SetupSerial();
	
	puts("Starting sensor calibration");
	
	Sensors->CalibrateSensors();
	
	puts("Ending sensor calibration");
	
	Kinematics->setStartingAngles();
	FlightControl->setInitialAltitude(Sensors->getAltitude());
	
	struct timeval start, cur;
	gettimeofday(&start, NULL);

	while(1)
	{
		Sensors->PollSensors();
		Sensors->MeasureSensors();
		Kinematics->Update();
		FlightControl->Update();
		Network->process();
		EventInterface->processEvents();
		
		gettimeofday(&cur, NULL);

		struct timeval diff;
		timeval_subtract(&diff, &cur, &start);
		if(_difftime(diff, 10))
		{
			start = cur;
			Net::Error err;
			char message[256];

//			sprintf(message, "Kinematics Pitch: %.2f  Yaw: %.2f  Roll: %.2f\n", Kinematics->getPitch(), Kinematics->getYaw(), Kinematics->getRoll());

			printf("Kinematics Pitch: %.2f  Yaw: %.2f  Roll: %.2f\n", Kinematics->getPitch(), Kinematics->getYaw(), Kinematics->getRoll());
			
//			FlightControl->printCorrections();
			
//			printf("Process count: %d\n", Sensors->processCount);
			Sensors->processCount = 0;
			
//			PacketMsg msg(message, strlen(message));
			if(clientSock != InvalidSocket)
			{
//				err = Network->send(clientSock, msg.getPacketBuffer(), msg.getPacketSize());
			}
		}
	}
	
	Network->shutdown();
	delete MotorController;
	delete Sensors;
	delete Kinematics;

	return 0;
}
