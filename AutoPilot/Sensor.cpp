#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <termios.h>
#include <vector>
#include <fstream>
#include <unistd.h>
#include <sstream>

#include "fcntl.h"

#include "Sensor.h"
#include "Util.h"

SensorController *Sensors = NULL;


Gyroscope::Gyroscope()
{
//	gyroScaleFactor = Radian(1.0f / 14.375f);
	gyroScaleFactor = 1.0f / 14.375f;
}

Gyroscope::~Gyroscope()
{

}

void Gyroscope::Measure()
{
	Angle.x = RawAngle.x - Zero.x;
	Angle.y = RawAngle.y - Zero.y;
	Angle.z = Zero.z - RawAngle.z;
}

void Gyroscope::Calibrate(float &zeroX, float &zeroY, float &zeroZ)
{
	zeroX = RawAngle.x;
	zeroY = RawAngle.y;
	zeroZ = RawAngle.z;
}

void Gyroscope::Finalize(float *zeroX, float *zeroY, float *zeroZ)
{
	Zero.z = findMedianFloat(zeroX, SAMPLECOUNT);
	Zero.y = findMedianFloat(zeroY, SAMPLECOUNT);
	Zero.z = findMedianFloat(zeroZ, SAMPLECOUNT);
}

Accelerometer::Accelerometer()
{

}

Accelerometer::~Accelerometer()
{

}

void Accelerometer::Measure()
{
	Angle.x = RawAngle.x + Bias.x;
	Angle.y = RawAngle.y + Bias.y;
	Angle.z = RawAngle.z + Bias.z;	
}

void Accelerometer::Calibrate()
{
	AccelSum.x += RawAngle.x;
	AccelSum.y += RawAngle.y;
	AccelSum.z += RawAngle.z;
}

void Accelerometer::Finalize()
{
	Angle.x = (float(AccelSum.x) / SAMPLECOUNT);
	Angle.y = (float(AccelSum.y) / SAMPLECOUNT);
	Angle.z = (float(AccelSum.z) / SAMPLECOUNT);

	Bias.x = -Angle.x;
	Bias.y = -Angle.y;
	Bias.z = -9.8065 - Angle.z;

	accelOneG = abs(Angle.z + Bias.z); // acc_1G = 265;
}

Magnetometer::Magnetometer()
{

}

Magnetometer::~Magnetometer()
{

}

void Magnetometer::Calibrate()
{

}

void Magnetometer::Finalize()
{

}

void Magnetometer::Measure(float roll, float pitch)
{
  Angle.x = RawAngle.x + Bias.x;
  Angle.y = RawAngle.y + Bias.y;
  Angle.z = RawAngle.z + Bias.z;

  const float cosRoll =  cos(roll);
  const float sinRoll =  sin(roll);
  const float cosPitch = cos(pitch);
  const float sinPitch = sin(pitch);

  const float magX = (float)Angle.x * cosPitch + 
                     (float)Angle.y * sinRoll * sinPitch + 
                     (float)Angle.z * cosRoll * sinPitch;
           
  const float magY = (float)Angle.y * cosRoll - 
                     (float)Angle.z * sinRoll;

  const float tmp  = sqrt(magX * magX + magY * magY);
   
  HeadingX = magX / tmp;
  HeadingY = -magY / tmp;
}

SensorController::SensorController()
{
	first_run = true;
	
	mGyro = new Gyroscope();
	mAccel = new Accelerometer();
	mMagnet = new Magnetometer();
	
	Altitude = 0.0;
	processCount = 0;
	
	last_str = "";
}

SensorController::~SensorController()
{
	delete mGyro;
	delete mAccel;
	delete mMagnet;
}

void SensorController::SetupMuxing()
{
	// Pin muxing
	system("echo 0x8000 > /sys/kernel/debug/omap_mux/uart1_txd");
	system("echo 0x8020 > /sys/kernel/debug/omap_mux/uart1_rxd");
	
	/*
	std::ofstream txd("/sys/kernel/debug/omap_mux/uart1_txd");
	std::ofstream rxd("/sys/kernel/debug/omap_mux/uart1_rxd");
	
	txd << 0x8000 << std::endl;
	rxd << 0x8020 << std::endl;
	*/
}

void SensorController::SetupSerial()
{
	serialHandle = open("/dev/ttyO1",O_RDWR|O_NOCTTY|O_NONBLOCK);
 
	if(serialHandle == 0)
	{
		puts("Serial Port Open - Status Fail");
	}
	else
	{
		struct termios options;
	
		puts("Serial Port Open - Status Pass");
		
		fcntl(serialHandle, F_SETFL, FNDELAY);
	
		tcgetattr(serialHandle, &options); /* Get the current options for the port */
		
		cfsetispeed(&options, B115200); /* Set the baud rates to 115200 */
		cfsetospeed(&options, B115200);
		options.c_cflag |= (CLOCAL | CREAD); /* Enable the receiver and set local mode */
		options.c_cflag &= ~PARENB; /* Mask the character size to 8 bits, no parity */
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8; /* Select 8 data bits */
		options.c_cflag &= ~CRTSCTS; /* Disable hardware flow control */
		options.c_lflag &= ~(ICANON | ECHO | ISIG);/* Enable data to be processed as raw input */

		/*
		options.c_iflag &= ~(IGNPAR | IXON | IXOFF);
		options.c_iflag |= IGNPAR;

		options.c_cflag &= ~(CSIZE | PARENB | CSTOPB | CREAD | CLOCAL);
		options.c_cflag |= CS8;
		options.c_cflag |= CREAD;
		options.c_cflag |= CLOCAL;

		options.c_lflag &= ~(ICANON | ECHO);
		*/
		options.c_cc[VMIN] = 0;
		options.c_cc[VTIME] = 0;		
		
		
		tcsetattr(serialHandle, TCSANOW, &options); /* Set the new options for the port */
	}
	
	usleep (2000000);
}

void SensorController::MeasureSensors()
{
	mAccel->Measure();
	mGyro->Measure();
}

void SensorController::CalibrateSensors()
{
	float gyroZero[3][SAMPLECOUNT];
	float TotalAltitude = 0.0;
	
	for(int i = 0, gyro = 0; i < SAMPLECOUNT; i++, gyro++)
	{
		PollSensors();
	
//		mAccel->Calibrate();
		
//		mGyro->Calibrate(gyroZero[0][gyro], gyroZero[1][gyro], gyroZero[2][gyro]);
		
		TotalAltitude += Altitude;
		
//		usleep(2000);
	}
	
	puts("Finalizing calibration");
	
//	mAccel->Finalize();
//	mGyro->Finalize(gyroZero[0], gyroZero[1], gyroZero[2]);
	
	AltitudeCalibrated = TotalAltitude / SAMPLECOUNT;
	
	//puts("Calibrarting magnetometer for 30 seconds");
	
	//mMagnet->Calibrate();
	//mMagnet->Finalize();
	
	//puts("Calibration finished");
}

void SensorController::PollSensors()
{
	char szBuff[1];
	 
	int numtemp;
	
	std::string str;
	
	numtemp = read(serialHandle, szBuff, 1);
	
	if( numtemp > 0 )
	{
		if(szBuff[0] == '!' && last_str.length() > 1)
		{
//			printf("Line: %s\n", last_str.c_str());
			process_string(last_str);
			last_str.erase(0, last_str.length());
		}
		else
		{
			str = szBuff[0];
			last_str.append(str);
		}
	}
	
	/*
	numtemp = read(serialHandle, &szBuff, sizeof(szBuff));
	
	if(numtemp > 0)
	{
		str.append(szBuff);
		
		std::istringstream stream(str);
		std::string line;
		unsigned int linec = 0;
		int processed = 0;
		
		while( std::getline(stream, line) )
		{
			if(line.length() < 4)
				continue;
			
			linec++;
			
			std::size_t find = line.find("!ANG:");
			if(find == std::string::npos)
				continue;
			
			while( !isdigit(line.at(line.length()-1)))
			{
				line.erase(line.length()-1);
				if(line.length() <= 1)
					break;
			}
		
			processed++;
//			printf("Process line: %s\n", line.c_str());
			process_string(line);
		}
		
		printf("%d lines, %d processed\n", linec, processed);
		
	
	}
	
	
	/*
	if(numtemp > 0)
	{
//		printf("New data: %s\n", szBuff);
		if(first_run == true)
		{
			last_str.append(szBuff);
			
//			printf("Formed string: %s\n", last_str.c_str());
			
			size_t n = last_str.find_last_of(':') + 1;
			while(n < last_str.length())
			{
				if( ! isdigit(last_str.at(n)) && last_str.at(n) != ',' )
				{
					last_str.erase(n, last_str.length() - n);
					break;
				}
				n++;
			}			
			
			//while( !isdigit(last_str.at(last_str.length()-1)))
			//	last_str.erase(last_str.length()-1);
			
//			printf("Fixed: %s\n", last_str.c_str());
			
			std::size_t found = std::string::npos;
			while( (found = last_str.find("!")) != std::string::npos)
			{
//				printf("%s\n", last_str.c_str());
				std::size_t foundTwo = last_str.find("!", found+1, 1);
				if(foundTwo != std::string::npos)
				{
//					printf("Found: %s\n", last_str.substr(found, (foundTwo - 1)).c_str());
					std::string proc = last_str.substr(found, (foundTwo - 1));
	//				if(proc.at(1) == 'A' && proc.at(2) == 'N' && proc.at(3) == 'G')
						process_string(proc);
					last_str.erase(0, foundTwo - 1);
//					printf("After: %s\n", last_str.c_str());
				}
				else
				{
					
//					break;
//					printf("Erasing rest of string: %s\n",last_str.c_str());
//					last_str.erase(0);
					break;
//					last_str = "";
				}
			}
		}
		else
		{
		
		}
		
		memset(szBuff, 0, sizeof(szBuff));
	}
	
	*/
	
	/*
	if (szBuff != 0 && numtemp > 0)
	{
		char *pch;
		pch = strtok(szBuff, "!");
		str = pch;
		if(str.at(0) == 'A' && str.at(1) == 'N')
		{
			if(first_run == false)
				process_string(last_str);
			else
				process_string(str);
			last_str = str;
		}
		else if(pch != NULL)
		{
			if(first_run == false)
				last_str.append(str);
		}
//		printf("Start: %s\n", str.c_str());
		while(pch != NULL)
		{
			pch = strtok(NULL, "!");
			if(pch != NULL)
			{
				str = pch;
//				printf("Next: %s\n", str.c_str());
				unsigned pos = str.find("@");
				if(str.length() < 2)
					continue;
				if( pos != std::string::npos )
					str.erase(pos);
				if(str.at(0) == 'A' && str.at(1) == 'N')
				{
					if(first_run == false)
						process_string(last_str);
					else
						process_string(str);
					last_str = str;
				}
				else
				{
					last_str.append(str);
				}
			}
		}
	
		memset(szBuff, 0, sizeof(szBuff));
		
		if(first_run)
			first_run = false;
	}
	*/
}

void SensorController::process_string(std::string str)
{
//	printf("Processing: %s\n", str.c_str());

	std::vector<char*> tokens;
	char *pch;
	
	pch = strtok((char*)str.c_str(), ",");
	tokens.push_back(pch);

	while(pch != NULL)
	{
		pch = strtok(NULL, ",");
		tokens.push_back(pch);
	}
	
//	printf("Token count: %d\n", tokens.size());
	
	if(tokens.size() != 7)
		return;
	
	Roll = atof(tokens[1]);
	Pitch = atof(tokens[2]);
	Yaw = atof(tokens[3]);
	
//	printf("Roll: %.2f, Pitch: %.2f, Yaw: %.2f\n", Roll, Pitch, Yaw);

//	mAccel->Angle.x = atof(tokens[5]);
//	mAccel->Angle.y = atof(tokens[6]);
//	mAccel->Angle.z = atof(tokens[7]);

//	mGyro->Angle.x = atof(tokens[8]);
//	mGyro->Angle.y = atof(tokens[9]);
//	mGyro->Angle.z = atof(tokens[10]);
	
//	mMagnet->Angle.x = atof(tokens[11]);
//	mMagnet->Angle.y = atof(tokens[12]);
	//mMagnet->Angle.z = atof(tokens[13]);
//	mMagnet->Heading = atof(tokens[14]);
	
	Temperature = (float)atoi(tokens[4]) / 10; // Temperature has to be divided by 10 to be turned into a proper float value
	Pressure = atoi(tokens[5]);
	
	if(Pressure > 102500)
		Pressure = 102500; // Highest we expect to see
	
	float baroRawAltitude = 44330 * (1 - pow((float)Pressure/101325.0, 1/5.255));
	Altitude = (baroRawAltitude * (1.0 - 0.02) + (Altitude * 0.02)); 
	
	processCount++;
//	puts("String processed\n");
	
//	mAccel->RawAngle.x = atof(tokens[18]);
//	mAccel->RawAngle.y = atof(tokens[19]);
//	mAccel->RawAngle.z = atof(tokens[20]);

//	mGyro->RawAngle.x = atof(tokens[21]);
//	mGyro->RawAngle.y = atof(tokens[22]);
//	mGyro->RawAngle.z = atof(tokens[23]);
	
//	mMagnet->RawAngle.x = atof(tokens[24]);
//	mMagnet->RawAngle.y = atof(tokens[25]);
//	mMagnet->RawAngle.z = atof(tokens[26]);	

//	printf("%s\n", str.c_str());

//	printf("Pressure: %d, Altitude: %.2f, Temperature: %.1f\n", Pressure, Altitude, Temperature);
//	printf("- Pitchs: %.2f  Yaw: %.2f  Roll: %.2f\n", Pitch, Yaw, Roll);
}