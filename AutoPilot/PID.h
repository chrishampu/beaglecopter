#ifndef _PID_H_
#define _PID_H_

class PID
{
public:
	PID();
	~PID();
	
	void initGain(float _kP, float _kI, float _kD);
	
	float getPTerm(float error);
	float getITerm(float error, float dt);
	float getDTerm(float error, float dt);
	
	float getPIDTerm(float error, float dt);
	
private:
	float integrator;
	float lastDerivative;
	float lastError;
	
	float kP;
	float kI;
	float kD;
};

#endif