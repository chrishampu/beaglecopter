#ifndef _PACKET_MOTOR_H_
#define _PACKET_MOTOR_H_

#include "NetPacket.h"

static enum MotorCommand
{
	AllMotors = 0,
	ArmMotor,
	RunMotor,
	StopMotor,
	AdjustThrottle
} MotorCmds;

class PacketMotor : public NetPacket
{
public:
    PacketMotor();
    PacketMotor(MotorCommand command, short motor, short arg);

    virtual void PreparePacket();

    virtual void Handle(NetStream& ns);

    virtual void Decode(NetStream& ns);

    MotorCommand getCommand();
    short getAffectedMotor();
	short getArg();

private:
    MotorCommand command;
    unsigned short motor;
	unsigned short arg;
};

#endif
