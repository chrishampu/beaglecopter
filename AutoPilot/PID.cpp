#include "PID.h"

/*
kP, kI, kD are gain values
dt is delta time between loops

error = desired value - actual value
        (target)      - (actual)
		
#define TEST_P 1.0
#define TEST_I 0.01
#define TEST_D 0.2
#define TEST_IMAX 10
*/

const float filter = 7.9577e-3;
const float WindupGuardGain = 5;

PID::PID()
{
	integrator = 0;
	lastDerivative = 0;
	lastError = 0;
}

PID::~PID()
{
}

void PID::initGain(float _kP, float _kI, float _kD)
{
	kP = _kP;
	kI = _kI;
	kD = _kD;
}

float PID::getPTerm(float error)
{
	return error * kP;
}

float PID::getITerm(float error, float dt)
{
    if((kI != 0) && (dt != 0))
	{
		float WindupGuard = WindupGuardGain / kI;
		
        integrator += (error * kI) * dt;
		
        if (integrator < -WindupGuard) {
            integrator = -WindupGuard;
        } else if (integrator > WindupGuard) {
            integrator = WindupGuard;
        }
        return integrator;
    }
    return 0;
}

float PID::getDTerm(float error, float dt)
{
    if ((kD != 0) && (dt != 0)) {
        float derivative;
		
		if (lastDerivative == 0) {
				// Happens after a reset
				derivative = 0;
				lastDerivative = 0;
		} else {
				derivative = (error - lastError) / dt;
		}

		// Low pass filter that cuts out high frequency noise
        derivative = lastDerivative + (dt / (filter + dt)) * (derivative - lastDerivative);

        lastError = error;
        lastDerivative = derivative;

        return kD * derivative;
    }
    return 0;
}

float PID::getPIDTerm(float error, float dt)
{
	return getPTerm(error) + getITerm(error, dt) + getDTerm(error, dt);
}