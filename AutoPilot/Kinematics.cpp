#include "Kinematics.h"

#include <stdio.h>

Stabilization *Kinematics = NULL;

Stabilization::Stabilization()
{
	Orientation = Quaternion::IDENTITY;
	
	startingYaw = 0;
	startingPitch = 0;
	startingRoll = 0;	
}

Stabilization::~Stabilization()
{

}

void Stabilization::Update()
{
	// Quaternions wont suffer from gimbal lock, so set our angles to real 0 ~ 360 in XYZ

	Degree y(Sensors->getYaw());
	Degree p(Sensors->getPitch());
	Degree r(Sensors->getRoll()));
	
	Quaternion quatR, quatY, quatP;
	quatY.FromAngleAxis(y, Vector3::UNIT_Y);
	quatP.FromAngleAxis(p, Vector3::UNIT_X);
	quatR.FromAngleAxis(r, Vector3::UNIT_Z);
	
	Orientation = quatY * quatP * quatR;
	
//	yaw = Orientation.getYaw().valueDegrees();
//	pitch = Orientation.getPitch().valueDegrees();
//	roll = Orientation.getRoll().valueDegrees();
	
	yaw = y.valueDegrees() - startingYaw;
	pitch = p.valueDegrees() - startingPitch;
	roll = r.valueDegrees() - startingRoll;
	
//	printf("Kinematics Pitch: %.2f  Yaw: %.2f  Roll: %.2f", pitch, yaw, roll);
}

void Stabilization::setStartingAngles()
{
	Degree y(Sensors->getYaw());
	Degree p(Sensors->getPitch());
	Degree r(Sensors->getRoll()));
	
	startingYaw = y.valueDegrees();
	startingPitch = p.valueDegrees();
	startingRoll = r.valueDegrees();
}