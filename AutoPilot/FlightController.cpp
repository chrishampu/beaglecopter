#include "FlightController.h"
#include "Sensor.h"
#include "Kinematics.h"
#include <stdio.h>
#include <time.h>


FlightController *FlightControl = NULL;

FlightController::FlightController()
{
	InitialAltitude = 0;

	yawPID.initGain(1.0, 0.01, 0.2);
	pitchPID.initGain(1.0, 0.01, 0.2);
	rollPID.initGain(1.0, 0.01, 0.2);
	
	clock_gettime(CLOCK_MONOTONIC, &curTime);
}

FlightController::~FlightController()
{

}

void FlightController::Update()
{
    oldTime = curTime;
    clock_gettime(CLOCK_MONOTONIC, &curTime);
    deltaTime = ((static_cast<long long int>(curTime.tv_sec) * 1000000000 + static_cast<long long int>(curTime.tv_nsec)) - (static_cast<long long int>(oldTime.tv_sec) * 1000000000 + static_cast<long long int>(oldTime.tv_nsec))) / 1000000000.0;

	// For stabilizing, we want to target having all angles at 0
	yaw = yawPID.getPIDTerm(0 - Kinematics->getYaw(), deltaTime);
	pitch = pitchPID.getPIDTerm(0 - Kinematics->getPitch(), deltaTime);
	roll = rollPID.getPIDTerm(0 - Kinematics->getRoll(), deltaTime);
}

void FlightController::printCorrections()
{
	printf("Correction for pitch: %.2f, yaw: %.2f, roll: %.2f,    delta: %.8f\n", pitch, yaw, roll, deltaTime);
}