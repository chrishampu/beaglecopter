#ifndef _EVENT_H_
#define _EVENT_H_


enum EventTypes
{
	Invalid,
	ConsoleEventType,
	PacketReceivedEventType,
	ConnectionReceivedEventType,
	ConnectionAcceptedEventType,
	ConnectionNotifyEventType
};

enum EventConstants
{
	MaxConsoleLineSize = 512,
	MaxPacketDataSize = 1024
};

struct Event
{
	short type, size;

	Event() { size = sizeof(Event); }
};

struct ConsoleEvent : public Event
{
	char payload[MaxConsoleLineSize];

	ConsoleEvent() { type = ConsoleEventType; size = sizeof(ConsoleEvent); }
};

struct PacketReceivedEvent : public Event
{
	//NetStream stream;
	unsigned char payload[MaxPacketDataSize];
	int len;

	PacketReceivedEvent() { type = PacketReceivedEventType; size = sizeof(PacketReceivedEvent); }
};

struct ConnectionNotifyEvent : public Event
{

	ConnectionNotifyEvent() { type = ConnectionNotifyEventType; size = sizeof(ConnectionNotifyEvent); }
};

struct ConnectionReceivedEvent : public Event
{

	ConnectionReceivedEvent() { type = ConnectionReceivedEventType; size = sizeof(ConnectionReceivedEvent); }
};

struct ConnectionAcceptedEvent : public Event
{

	ConnectionAcceptedEvent() { type = ConnectionAcceptedEventType; size = sizeof(ConnectionAcceptedEvent); }
};

#endif