#include "Util.h"

bool _difftime(struct timeval t1, int milliseconds)
{
	return (((t1.tv_sec) * 1000 + (t1.tv_usec) / 1000) > milliseconds);
}

void timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1)
{
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;
}

float findMedianFloat(float *data, int arraySize) 
{
  float temp;
  bool done = 0;
  int i;
  
   // Sorts numbers from lowest to highest
  while (done != 1) 
  {        
    done = 1;
    for (i=0; i<(arraySize-1); i++) 
	{
      if (data[i] > data[i+1]) 
	  {     // numbers are out of order - swap
        temp = data[i+1];
        data[i+1] = data[i];
        data[i] = temp;
        done = 0;
      }
    }
  }
  
  return data[arraySize/2]; // return the median value
}

int findMedianInt(int *data, int arraySize) 
{
  int temp;
  bool done = 0;
  int i;
  
   // Sorts numbers from lowest to highest
  while (done != 1) 
  {        
    done = 1;
    for (i=0; i<(arraySize-1); i++) 
	{
      if (data[i] > data[i+1]) 
	  {     // numbers are out of order - swap
        temp = data[i+1];
        data[i+1] = data[i];
        data[i] = temp;
        done = 0;
      }
    }
  }
  
  return data[arraySize/2]; // return the median value
}
