#ifndef _UTIL_H_
#define _UTIL_H_

#include <sys/time.h>
#include <math.h>

bool _difftime(struct timeval t1, int milliseconds);
void timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1);
float findMedianFloat(float *data, int arraySize); 
int findMedianInt(int *data, int arraySize);

#endif
