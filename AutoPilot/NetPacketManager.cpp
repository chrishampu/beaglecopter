#include "NetPacketManager.h"

#include <stdio.h>
#include <map>

std::map<unsigned short, PacketHandle> PacketList;

PacketControl *PacketManager = NULL;

/**
	Macro for defining new packets so they can be processed when received.
*/
#define IMPLEMENT(x) \
	void Handle_Packet ##x (NetStream& b); \
	static PacketControl Control_Packet ##x(pkt ##x, Handle_Packet ##x); \
	void Handle_Packet ##x (NetStream& b) \
	{ \
		Packet ##x *p; \
		p = new Packet ##x (); \
		p->Handle(b); \
		if(p != NULL ) { \
			delete p; \
			p = NULL; \
		} \
	}

/**
	And here we implement all of our packets.
*/
IMPLEMENT(Msg)
IMPLEMENT(Motor)


PacketControl::PacketControl()
{

}

PacketControl::~PacketControl()
{
	PacketList.clear();
}

PacketControl::PacketControl(unsigned short id, PacketHandle handle)
{
	PacketManager->Register(id, handle);
}

void PacketControl::processPacketReceivedEvent(PacketReceivedEvent *event)
{
	unsigned short packetID;
	NetStream stream; // Create a temporary stream
	
	stream.write(event->payload, event->len); // Copy the event data into our stream

	stream.peek((unsigned char*)&packetID, 2); // Now we can peek into our stream to grab our packet ID

	std::map<unsigned short, PacketHandle>::iterator itr;
	itr = PacketList.find(packetID); // Attempt to find a handle that matches our packet ID

	if(itr == PacketList.end()) // No handle? Dump the packet
		return;

	PacketHandle handle = itr->second; // Grab our handle

	(*handle)(stream); // Run our handle callback!
}

void PacketControl::Register(unsigned short id, PacketHandle handle)
{
	PacketList[id] = handle;
}

void PacketControl::Create()
{
	if(PacketManager == NULL)
		PacketManager = new PacketControl();
}

void PacketControl::Destroy()
{
	if(PacketManager != NULL)
		delete PacketManager;
}
