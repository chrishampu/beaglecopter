#ifndef _NETPACKETMANAGER_H_
#define _NETPACKETMANAGER_H_

#include "NetPacket.h"
#include "NetPacketList.h"
#include "Event.h"

#include <map>

typedef void (*PacketHandle)(NetStream& b);

class PacketControl
{
public:
    PacketControl();
    ~PacketControl();

    PacketControl(unsigned short id, PacketHandle handle);

    void processPacketReceivedEvent(PacketReceivedEvent *event);

    void Register(unsigned short id, PacketHandle handle);

    void Create();
    void Destroy();
};

extern PacketControl* PacketManager;

#endif