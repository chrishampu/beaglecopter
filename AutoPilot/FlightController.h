#ifndef _FLIGHT_CONTROLLER_H_
#define _FLIGHT_CONTROLLER_H_

#include "PID.h"
#include <time.h>

enum FlightState
{
	Disarmed,
	TakeOff,
	Stabilize,
	Landing
}

class FlightController
{
public:
	FlightController();
	~FlightController();
	
	void Update();
	void printCorrections();
	
	void setInitialAltitude(float Altitude) { InitialAltitude = Altitude; }
	float getInitialAltitude() { return InitialAltitude; }
	
private:
	float InitialAltitude;
	float deltaTime;

	PID yawPID;
	PID pitchPID;
	PID rollPID;
	
    timespec oldTime;
    timespec curTime;
	
	float pitch;
	float yaw;
	float roll;
};

extern FlightController *FlightControl;

#endif