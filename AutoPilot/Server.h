#ifndef _SERVER_H_
#define _SERVER_H_

struct NetAddress {
   int type;        ///< Type of address (IPAddress currently)

   /// Acceptable NetAddress types.
   enum {
      IPAddress,
      IPXAddress
   };

   unsigned char netNum[4];    ///< For IP:  sin_addr<br>
                    ///  For IPX: sa_netnum

   unsigned short nodeNum[6];   ///< For IP:  Not used.<br>
                    ///  For IPX: sa_nodenum (not used by IP)

   unsigned short  port;       ///< For IP:  sin_port<br>
                    ///  For IPX: sa_socket
};

typedef int NetSocket;
const NetSocket InvalidSocket = -1;

class Net
{
public:
   enum Error
   {
      NoError,
      WrongProtocolType,
      InvalidPacketProtocol,
      WouldBlock,
      NotASocket,
      UnknownError
   };

   enum Protocol
   {
      UDPProtocol,
      IPXProtocol,
      TCPProtocol
   };

   bool init();
   void shutdown();

//   Error sendto(const NetAddress *address, const unsigned char *buffer, unsigned int bufferSize);

   // Reliable net functions (TCP)
   // all incoming messages come in on the Connected* events
   NetSocket openListenPort(unsigned short port);
   NetSocket openConnectTo(const char *stringAddress); // does the DNS resolve etc.
   void closeConnectTo(NetSocket socket);
   Error sendtoSocket(NetSocket socket, const unsigned char *buffer, unsigned int bufferSize);

   void process();

//   bool compareAddresses(const NetAddress *a1, const NetAddress *a2);
//  bool stringToAddress(const char *addressString, NetAddress *address);
//   void addressToString(const NetAddress *address, char addressString[256]);

   // lower level socked based network functions
   NetSocket openSocket();
   Error closeSocket(NetSocket socket);

   Error connect(NetSocket socket, const NetAddress *address);
   Error listen(NetSocket socket, int maxConcurrentListens);
   NetSocket accept(NetSocket acceptSocket, NetAddress *remoteAddress);

   Error bind(NetSocket socket, unsigned short port);
   Error setBufferSize(NetSocket socket, unsigned int bufferSize);
   Error setBroadcast(NetSocket socket, bool broadcastEnable);
   Error setBlocking(NetSocket socket, bool blockingIO);

   Error send(NetSocket socket, const unsigned char *buffer, unsigned int bufferSize);
   Error recv(NetSocket socket, unsigned char *buffer, int bufferSize, int *bytesRead);
};

extern Net *Network;

#endif