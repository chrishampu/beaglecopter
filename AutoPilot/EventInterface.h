#ifndef _EVENTINTERFACE_H_
#define _EVENTINTERFACE_H_

#include "Event.h"
#include <vector>
#include "NetStream.h"
//#include "netStream.h"

class EventQueueInterface
{
public:
    EventQueueInterface();

    void Initialize();

    void packEvent(Event &ev);
    void processEvents();

//    void processConsoleEvent(ConsoleEvent *event);
    void processPacketReceivedEvent(PacketReceivedEvent *event);

private:
    std::vector<Event*> eventQueue1, *eventQueue;
    bool running;
};

extern EventQueueInterface *EventInterface;

#endif