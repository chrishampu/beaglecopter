#include "PacketMotor.h"
#include "Motor.h"

PacketMotor::PacketMotor()
{
	packetID = pktMotor;
	packetSize = 2;
}

PacketMotor::PacketMotor(MotorCommand command, short motor, short arg)
{
	packetID = pktMotor;

	this->command = command;
	this->motor = motor;
	this->arg = arg;
	
	setPacketSize(2 + 2 + 2 + 2); // command (byte) + motor + arg + packetID

	PreparePacket();
}

void PacketMotor::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << (short)command;
	PacketData << motor;
	PacketData << arg;
}

void PacketMotor::Handle(NetStream& ns)
{
	Decode(ns);
	
	printf("Command %d receieved affecting motor %d with arg %d\n", command, motor, arg);
	
	
	switch(command)
	{
		case ArmMotor:
			if(motor)
				MotorController->Arm((Motors)motor);
			else
				MotorController->Arm();
			break;
		case RunMotor:
			if(motor)
				MotorController->Run((Motors)motor);
			else
				MotorController->Run();
			break;
		case StopMotor:
			if(motor)
				MotorController->Stop((Motors)motor);
			else
				MotorController->Stop();
			break;
		case AdjustThrottle:
			if(motor)
				MotorController->setSpeed((Motors)motor, arg);
			else
				MotorController->setSpeed(arg);
			break;
	}
}

void PacketMotor::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	unsigned short com;

	ns >> com;
	command = (MotorCommand)com;
	ns >> motor;
	ns >> arg;

	if(motor < 1 || motor > 4)
		motor = 0;

	if(arg < 1 || arg > 100)
		arg = 0;
}

MotorCommand PacketMotor::getCommand()
{
	return command;
}

short PacketMotor::getAffectedMotor()
{
	return motor;
}
short PacketMotor::getArg()
{
	return arg;
}
