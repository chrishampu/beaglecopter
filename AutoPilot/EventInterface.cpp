#include "EventInterface.h"
#include "NetPacketManager.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>

EventQueueInterface *EventInterface = NULL;

EventQueueInterface::EventQueueInterface()
{
	if(EventInterface != NULL)
	{
		printf("Fatal error: EventInterface already instantiated!\n");
		return;
	}

	EventInterface = this;

	eventQueue = &eventQueue1;
}

void EventQueueInterface::Initialize()
{
	if(EventInterface == NULL)
		EventInterface = new EventQueueInterface();
}

void EventQueueInterface::packEvent(Event &event)
{
	// Save a deep copy of our event
	Event* copy = (Event*)malloc(event.size);
	memcpy(copy, &event, event.size);

	// Send the event to the queue
	eventQueue->push_back(copy);
}

void EventQueueInterface::processEvents()
{
	std::vector<Event*> &fullEventQueue = *eventQueue; // Make a reference copy of our event queue

	for(unsigned int i = 0; i < eventQueue->size(); i++)
	{
		struct Event *ev = fullEventQueue[i];

		switch(ev->type)
		{
			case ConsoleEventType:
//				processConsoleEvent((ConsoleEvent*) ev);
				break;
			case PacketReceivedEventType:
				processPacketReceivedEvent((PacketReceivedEvent*) ev);
				break;
			case ConnectionReceivedEventType:
				break;
			case ConnectionAcceptedEventType:
				break;
			case ConnectionNotifyEventType:
				break;
			default:
				break;
		}

		free(fullEventQueue[i]); // Free the resource used by the event
	}

	fullEventQueue.clear(); // After iterating the queue, clear it
}

/*
void EventQueueInterface::processConsoleEvent(ConsoleEvent *event)
{
	Console->processConsoleEvent(event);
}
*/
void EventQueueInterface::processPacketReceivedEvent(PacketReceivedEvent *event)
{
	PacketManager->processPacketReceivedEvent(event);
}