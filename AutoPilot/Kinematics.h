#ifndef _KINEMATICS_H_
#define _KINEMATICS_H_

#include "Sensor.h"
#include "Quaternion.h"

class Stabilization
{
public:
	Stabilization();
	~Stabilization();
	
	void Update();
	
	void setStartingAngles();
	
	Quaternion& getOrientation() { return Orientation; }
	
	float getPitch() { return pitch; }
	float getYaw() { return yaw; }
	float getRoll() { return roll; }
	
	float getStartingPitch() { return startingPitch; }
	float getStartingYaw() { return startingYaw; }
	float getStartingRoll() { return startingRoll; }
	
private:
	Quaternion Orientation;
	float yaw;
	float pitch;
	float roll;
	
	float startingYaw;
	float startingPitch;
	float startingRoll;
};

extern Stabilization *Kinematics;

#endif